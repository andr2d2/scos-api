﻿namespace Scos.Entities
{
    public class TipoOrdemServico
    {
        public int id { get; set; }
        public string tipo { get; set; }

        public enum TipoOS
        {
            Garantia = 1,
            ContratoManutencao = 2,
            Orcamento = 3
        }
    }
}