﻿using System;
using System.Collections.Generic;

namespace Scos.Entities
{
    public class OrdemServico
    {
        public int id { get; set; }
        public int chamadoId { get; set; }
        public DateTime dataAberturaOs { get; set; }
        public DateTime? dataFechamentoOs { get; set; }
        public int tipoId { get; set; }
        public bool preventivo { get; set; }
        public int? tecnicoInicioAnalise { get; set; }
        public DateTime? dataInicioAnalise { get; set; }
        public string descricaoAnalise { get; set; }
        public int? tecnicoFimAnalise { get; set; }
        public DateTime dataFimAnalise { get; set; }
        public string descricaoConserto { get; set; }
        public int? tecnicoConserto { get; set; }
        public bool? autorizacao { get; set; }
        public string responsavelAutorizacao { get; set; }
        public DateTime? dataAutorizacao { get; set; }
        public DateTime? deslocamentoSaida { get; set; }
        public DateTime? deslocamentoChegada { get; set; }
        public DateTime? deslocamentoTotal { get; set; }
        public double? kmInicial { get; set; }
        public double? kmFinal { get; set; }
        public double? kmTotal { get; set; }
        public Status status { get; set; }
        public Empresa empresa { get; set; }
        public List<Item> item{ get; set; }
        public string defeitoInformado { get; set; }
        public Usuario usuario { get; set; }
        public List<Peca> pecas { get; set; }
    }
}