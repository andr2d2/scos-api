﻿namespace Scos.Entities
{
    public class Empresa
    {
        public int id { get; set; }
        public string cnpj { get; set; }
        public string nomeFantasia { get; set; }
        public string razaoSocial { get; set; }
        public string endereco { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string cep { get; set; }
        public Cidade cidade { get; set; }
        public Estado estado { get; set; }
        public string telContato { get; set; }
        public bool ativo { get; set; }
        public string municipio { get; set; }

        public Usuario usuarioInteracao{ get; set; }
    }
}
