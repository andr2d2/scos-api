﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scos.Entities
{
    public class Status
    {
        public int id { get; set; }
        public string status { get; set; }


        public enum enumStatus
        {
            Aberto = 1,
            Atribuido = 2,
            Planejado = 3,
            Fechado_FaltaAtendimento = 4,
            Fechado = 5
        }
    }
}
