﻿namespace Scos.Entities
{
    public class TipoChamado
    {
        public int id { get; set; }
        public string tipo { get; set; }

        public enum enumTipo
        {
            Garantia = 1,
            Normal = 2
        }
    }
}