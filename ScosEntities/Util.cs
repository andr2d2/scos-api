﻿using System;

namespace Scos.Entities
{
    public class Util
    {

        public double DateTimeToUnix(DateTime data)
        {
            var span = (double)(data - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime()).TotalSeconds;
            return span;
        }

        public DateTime UnixToDateTime(double time)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
            dtDateTime = dtDateTime.AddSeconds(time);
            return dtDateTime;
        }
    }
}
