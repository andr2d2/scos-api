﻿using System;
using Scos.DAL.Repository.Base;
using Scos.Entities;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Scos.DAL.Repository
{
    public class OrdemServicoRepository : Repository<tblOrdemServico>, IOrdemServicoRepository
    {
        // construtor para setar o contexto na classe base
        public OrdemServicoRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public int Cadastrar(OrdemServico os)
        {
            try
            {
                // info basica
                var osCadastrada = Add(new tblOrdemServico
                {
                    DataAberturaOs = DateTime.Now,
                    TipoId = os.tipoId,
                    Preventivo = os.preventivo,
                    EmpresaId = os.empresa.id,
                    DefeitoInformado = os.defeitoInformado.Trim()
                });

                ctx.SaveChanges();

                //itens da os
                foreach (var item in os.item)
                {
                    var tempItem = new tblItemOrdemServico()
                    {
                        DataInclusao = DateTime.Now,
                        ItemId = item.id,
                        OrdemServicoId = osCadastrada.Id
                    };

                    new Repository<tblItemOrdemServico>(ctx).Add(tempItem);
                }
                //usuario
                new Repository<tblUsuarioOrdemServico>(ctx).Add(
                    new tblUsuarioOrdemServico
                    {
                        DataAlteracao = DateTime.Now,
                        OrdemServicoId = osCadastrada.Id,
                        UsuarioId = os.usuario.id
                    });

                ctx.SaveChanges();

                return osCadastrada.Id;
            }
            //catch (Exception ex)
            //{
            //    return 0;
            //    throw;
            //}
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Debug.WriteLine("Erro ==================================================");
                    Debug.WriteLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Debug.WriteLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                    Debug.WriteLine("======================================================");
                }
                return 0;
                throw;
            }
        }

        public int Editar(OrdemServico os)
        {
            throw new NotImplementedException();
        }

        public int Fechar(int ordemServicoId)
        {
            throw new NotImplementedException();
        }

        public int InicioAnalise(OrdemServico os)
        {
            try
            {
                var temp = Get(os.id);
                temp.DataInicioAnalise = DateTime.Now;
                temp.DescricaoAnalise = os.descricaoAnalise.Trim();
                temp.TecnicoInicioAnalise = os.tecnicoInicioAnalise;

                Update(temp);

                return ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int FimAnalise(OrdemServico os)
        {
            try
            {
                var temp = Get(os.id);
                temp.DataFimAnalise = DateTime.Now;
                temp.TecnicoFimAnalise = os.tecnicoInicioAnalise;

                foreach (var p in os.pecas)
                {
                    var peca = new tblPecaOrdemServico
                    {
                        OrdemServicoId = os.id,
                        PecaId = p.id,
                        Quantidade = p.quantidade
                    };
                    new Repository<tblPecaOrdemServico>(ctx).Add(peca);
                }

                return ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int Autorizacao(OrdemServico os)
        {
            try
            {
                var temp = Get(os.id);
                temp.DataAutorizacao = DateTime.Now;
                temp.Autorizado = os.autorizacao;
                temp.ResponsavelAutorizacao = os.responsavelAutorizacao.Trim();
                
                return ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

    }
}
