﻿using Scos.DAL.Repository.Base;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Scos.DAL.Repository
{
    public class UsuarioRepository : Repository<tblUsuario>, IUsuarioRepository
    {
        // construtor para setar o contexto na classe base
        public UsuarioRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public int Cadastrar(Usuario usuario)
        {
            try
            {
                //empresa ativa
                var empresaAtiva = new Repository<tblEmpresa>(ctx).Get(usuario.empresa.id);

                if (empresaAtiva == null || !empresaAtiva.Ativo)
                    return 0;

                var novoUsuario = Add(new tblUsuario()
                {
                    Ativo = true,
                    Cpf = usuario.cpf.Trim(),
                    Email = usuario.email.Trim(),
                    EmpresaId = usuario.empresa.id,
                    NomeCompleto = usuario.nomeCompleto.Trim(),
                    TelContato = usuario.telContato.Trim(),
                    TipoId = usuario.tipousuario.id
                });

                ctx.SaveChanges();

                return novoUsuario.Id;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int Desativar(int id)
        {
            try
            {
                var usuario = Get(id);
                usuario.Ativo = false;
                Update(usuario);

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            };
        }

        public int Editar(Usuario usuario)
        {
            try
            {
                Update(new tblUsuario
                {
                    Ativo = usuario.ativo,
                    Cpf = usuario.cpf,
                    Email = usuario.email,
                    EmpresaId = usuario.empresa.id,
                    Id = usuario.id,
                    NomeCompleto = usuario.nomeCompleto,
                    TelContato = usuario.telContato,
                    TipoId = usuario.tipousuario.id
                });

                ctx.SaveChanges();

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public Usuario Login(Acesso acesso)
        {
            try
            {
                byte[] salt = Encoding.UTF8.GetBytes("igor viado!");
                var hashedPassword = HashPasswordWithSalt(Encoding.UTF8.GetBytes(acesso.senha), salt);
                var hash = Convert.ToBase64String(hashedPassword);
                
                var loginUser = (from a in ctx.tblAcesso
                                   join u in ctx.tblUsuario on a.UsuarioId equals u.Id
                                   where u.Ativo == true && a.Login == acesso.login && a.Chave == hash && a.PrimeiroAcesso == false
                                   select new Usuario { id = u.Id}).FirstOrDefault();

                if (loginUser != null && loginUser.id > 0)
                    return Usuario(loginUser.id);
                else
                    return new Usuario();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<TipoUsuario> TiposUsuario()
        {
            try
            {
                var listTipos = (from t in ctx.tblTipoUsuario
                                 select new TipoUsuario
                                 {
                                     id = t.Id,
                                     tipo = t.Tipo.Trim()
                                 }).ToList();

                return listTipos;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public TipoUsuario TipoUsuario(int id)
        {
            try
            {
                var tipo = new Repository<tblTipoUsuario>(ctx).Get(id);
                var tipoUsuario = new TipoUsuario
                {
                    id = tipo.Id,
                    tipo = tipo.Tipo.Trim()
                };

                return tipoUsuario;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Usuario Usuario(int id)
        {
            try
            {
                var usuario = (from u in ctx.tblUsuario
                               join t in ctx.tblTipoUsuario on u.TipoId equals t.Id
                               where u.Id == id
                               select new Usuario
                               {
                                   ativo = u.Ativo,
                                   cpf = u.Cpf,
                                   email = u.Email,
                                   empresa = new Empresa { id = u.EmpresaId },
                                   id = u.Id,
                                   nomeCompleto = u.NomeCompleto,
                                   telContato = u.TelContato,
                                   tipousuario = new TipoUsuario
                                   {
                                       id = t.Id,
                                       tipo = t.Tipo
                                   }
                               }).FirstOrDefault();

                return usuario;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<Usuario> Usuarios(int empresaId)
        {
            try
            {
                var usuarios = (from u in ctx.tblUsuario
                                join t in ctx.tblTipoUsuario on u.TipoId equals t.Id
                                join e in ctx.tblEmpresa on u.EmpresaId equals e.Id
                                where u.EmpresaId == empresaId && e.Ativo == true
                                select new Usuario
                                {
                                    ativo = u.Ativo,
                                    cpf = u.Cpf,
                                    email = u.Email,
                                    empresa = new Empresa { id = u.EmpresaId },
                                    id = u.Id,
                                    nomeCompleto = u.NomeCompleto,
                                    telContato = u.TelContato,
                                    tipousuario = new TipoUsuario
                                    {
                                        id = t.Id,
                                        tipo = t.Tipo
                                    }
                                }).ToList();

                return usuarios;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        private static byte[] Combine(byte[] first, byte[] second)
        {
            var ret = new byte[first.Length + second.Length];

            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);

            return ret;
        }

        private static byte[] HashPasswordWithSalt(byte[] toBeHashed, byte[] salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var combinedHash = Combine(toBeHashed, salt);

                return sha256.ComputeHash(combinedHash);
            }
        }
    }
}
