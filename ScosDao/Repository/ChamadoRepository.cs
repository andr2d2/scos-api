﻿using System;
using Scos.DAL.Repository.Base;
using Scos.Entities;
using System.Linq;
using System.Collections.Generic;
using Scos.DAL.Context;

namespace Scos.DAL.Repository
{
    public class ChamadoRepository : Repository<tblChamado>, IChamadoRepository
    {
        // construtor para setar o contexto na classe base
        public ChamadoRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public int Cadastrar(Chamado chamado)
        {
            try
            {
                // verifica se usuário é existente e ativo
                var usuarioExistente = new Repository<tblUsuario>(ctx).Get(chamado.usuario.id);

                if (usuarioExistente == null || !usuarioExistente.Ativo)
                    return 0;


                // adiciona chamado   
                var chamadoAdicionado = Add(new tblChamado()
                {
                    DataAbertura = DateTime.Now,
                    Descricao = chamado.descricao.Trim(),
                    TipoId = chamado.tipoChamado.id,
                    Ativo = true,
                    StatusId = (int)Status.enumStatus.Aberto
                });

                // itens do chamado se houver
                if (chamado.itens != null)
                {
                    foreach (var item in chamado.itens)
                    {
                        var itemAdicionado = new ItemRepository(ctx).Adicionar(item);
                        item.id = itemAdicionado.id;
                    }
                }


                // chamado ao usuario
                new Repository<tblChamadoUsuario>(ctx).Add(new tblChamadoUsuario
                {
                    ChamadoId = chamadoAdicionado.Id,
                    UsuarioId = chamado.usuario.id
                });

                ctx.SaveChanges();


                return chamadoAdicionado.Id;

            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public Chamado Chamado(int id)
        {
            try
            {
                var chamado = Chamados().Find(c => c.id == id);
                return chamado;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<Chamado> Chamados()
        {
            try
            {
                //var lstItem = (from itemCham in ctx.tblItemChamado
                //               join item in ctx.tblItem on itemCham.ItemId equals item.Id
                //               select new Item
                //               {
                //                   id = item.Id
                //               }).ToList();

                //var ordensServico = (from osCharm in ctx.tblChamadoOrdemServico
                //                     join os in ctx.tblOrdemServico on osCharm.OrdemServicoId equals os.Id
                //                     select new OrdemServico
                //                     {
                //                         id = os.Id
                //                     }).ToList();


                var listChamados = (from cham in ctx.tblChamado
                                    //join st in ctx.tblStatus on cham.StatusId equals st.Id
                                    join chamUser in ctx.tblChamadoUsuario on cham.Id equals chamUser.ChamadoId
                                    //join user in ctx.tblUsuario on chamUser.UsuarioId equals user.Id
                                    //join emp in ctx.tblEmpresa on user.EmpresaId equals emp.Id
                                    //join tp in ctx.tblTipoChamado on cham.TipoId equals tp.Id
                                    where cham.Ativo == true
                                    select new Chamado
                                    {
                                        //ativo = cham.Ativo,
                                        //dataAbertura = cham.DataAbertura,
                                        //dataFechamento = cham.DataFechamento,
                                        descricao = cham.Descricao.Trim(),
                                        id = cham.Id,
                                        status = new Status
                                        {
                                            id = cham.StatusId,
                                            //status = st.Status
                                        },
                                        tipoChamado = new TipoChamado
                                        {
                                            id = cham.TipoId.Value,
                                            //tipo = tp.Tipo
                                        },
                                        usuario = new Usuario
                                        {
                                            id = chamUser.UsuarioId,
                                            //nomeCompleto = user.NomeCompleto,
                                            //email = user.Email.Trim(),
                                            //empresa = new Empresa
                                            //{
                                            //    id = emp.Id,
                                            //    nomeFantasia = emp.NomeFantasia.Trim(),
                                            //    cnpj = emp.Cnpj.Trim(),
                                            //    bairro = emp.Bairro.Trim(),
                                            //    endereco = emp.Endereco.Trim(),
                                            //    cep = emp.Cep.Trim(),
                                            //    numero = emp.Numero.Trim(),
                                            //    complemento = emp.Complemento.Trim(),
                                            //    telContato = emp.TelContato.Trim()
                                            //}
                                        }
                                    }).ToList();
                                    //.AsEnumerable().Select(c => new Chamado {
                                    //    dataAbertura = new Util().DateTimeToUnix(c.dataAbertura),
                                    //    dataFechamento = (c.dataFechamento.HasValue ? new Util().DateTimeToUnix(c.dataFechamento.Value) : 0),
                                    //    itens = lstItem,
                                    //    ordensServico = ordensServico,
                                    //     usuario = c.usuario,
                                    //     tipoChamado = c.tipoChamado,
                                    //     ativo = c.ativo,
                                    //     descricao = c.descricao,
                                    //     id = c.id,
                                    //     status = c.status
                                    //}).ToList();
                

                return listChamados;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<Chamado> ChamadosPorEmpresa(int empresaId)
        {
            try
            {
                var chamados = Chamados().FindAll(c => c.usuario.empresa.id == empresaId);
                return chamados;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<Chamado> ChamadosPorUsuario(int usuarioId)
        {
            try
            {
                var chamados = Chamados().FindAll(c => c.usuario.id == usuarioId);
                return chamados;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public int Editar(Chamado chamado)
        {
            try
            {
                Update(new tblChamado
                {
                    Ativo = chamado.ativo,
                    Descricao = chamado.descricao,
                    TipoId = chamado.tipoChamado.id,
                    StatusId = chamado.status.id,
                    DataAbertura = new Util().UnixToDateTime(chamado.dataAbertura),
                    DataFechamento = null,
                    Id = chamado.id
                });

                ctx.SaveChanges();

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int Fechar(ChamadoUsuario chamadoUsuario)
        {
            try
            {
                // verifica OS abertas e as fecha
                var ordensServico = (from co in ctx.tblChamadoOrdemServico
                                     join os in ctx.tblOrdemServico on co.OrdemServicoId equals os.Id
                                     where co.ChamadoId == chamadoUsuario.chamadoId && os.StatusId != (int)Status.enumStatus.Aberto
                                     select new OrdemServico
                                     {
                                         id = os.Id
                                     }).ToList();

                foreach (var os in ordensServico)
                {
                    //fechar OS's
                }

                // fecha chamado
                var chamado = Get(chamadoUsuario.chamadoId);
                chamado.DataFechamento = DateTime.Now;
                chamado.StatusId = (int)Status.enumStatus.Fechado;
                chamado.Ativo = false;

                Update(chamado);

                ctx.SaveChanges();

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

    }
}
