﻿using System.Collections.Generic;
using System.Linq;
using Scos.DAL.Context;
using Scos.DAL.Repository.Base;
using Scos.Entities;

namespace Scos.DAL.Repository
{
    public class UsuarioRepository : Repository<tblUsuario>, IUsuarioRepository
    {
        // construtor para setar o contexto na classe base
        public UsuarioRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }
    }
}
