﻿using Scos.DAL.Repository.Base;
using Scos.Entities;

namespace Scos.DAL.Repository
{
    public interface IOrdemServicoRepository : IRepository<tblOrdemServico>
    {
        int Cadastrar(OrdemServico os);
        int Editar(OrdemServico os);
        int Fechar(int ordemServicoId);
        int InicioAnalise(OrdemServico os);
        int FimAnalise(OrdemServico os);
        int Autorizacao(OrdemServico os);
    }
}
