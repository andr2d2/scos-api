﻿using Scos.DAL;
using Scos.DAL.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;

namespace Scos.DAL.Repository
{
    public interface IChamadoRepository : IRepository<tblChamado>
    {
        int Cadastrar(Chamado chamado);
        int Editar(Chamado chamado);
        int Fechar(ChamadoUsuario chamadoUsuario);
        List<Chamado> Chamados();
        List<Chamado> ChamadosPorEmpresa(int empresaId);
        List<Chamado> ChamadosPorUsuario(int usuarioId);
        Chamado Chamado(int id);
    }
}
