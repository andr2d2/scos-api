﻿using Scos.DAL.Context;
using Scos.DAL.Repository.Base;

namespace Scos.DAL.Repository
{
    public class CidadeRepository : Repository<tblCidade>, ICidadeRepository
    {
        // construtor para setar o contexto na classe base
        public CidadeRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }
    }
}
