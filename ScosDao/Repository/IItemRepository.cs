﻿using Scos.DAL.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;

namespace Scos.DAL.Repository
{
    public interface IItemRepository : IRepository<tblItem>
    {                       
    	Item Adicionar(Item item);
    	List<Item> Itens();
        void Excluir(Item item);
        int Cadastrar(Item item);   	                 
    }
}
