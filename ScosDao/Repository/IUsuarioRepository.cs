﻿using Scos.DAL.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;

namespace Scos.DAL.Repository
{
    public interface IUsuarioRepository : IRepository<tblUsuario>
    {
        List<TipoUsuario> TiposUsuario();
        TipoUsuario TipoUsuario(int id);
        int Cadastrar(Usuario usuario);
        int Editar(Usuario usuario);
        int Desativar(int id);
        Usuario Usuario(int id);
        List<Usuario> Usuarios(int empresaId);
        Usuario Login(Acesso acesso);
    }
}