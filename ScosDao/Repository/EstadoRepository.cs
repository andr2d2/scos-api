﻿using Scos.DAL.Context;
using Scos.DAL.Repository.Base;

namespace Scos.DAL.Repository
{
    public class EstadoRepository : Repository<tblEstado>, IEstadoRepository
    {
        // construtor para setar o contexto na classe base
        public EstadoRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }
    }
}
