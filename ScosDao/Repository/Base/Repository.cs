﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Scos.DAL.Repository.Base
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext db;

        public Repository(DbContext context)
        {
            db = context;
        }


        public IEnumerable<TEntity> GetAll()
        {
            return db.Set<TEntity>().ToList();
        }

        public TEntity Get(int id)
        {
            return db.Set<TEntity>().Find(id);
        }

        public TEntity Find(Expression<Func<TEntity, bool>> predicate)
        {
            return db.Set<TEntity>().SingleOrDefault(predicate);
        }

        public TEntity Add(TEntity obj)
        {
            return db.Set<TEntity>().Add(obj);
        }

        public void Delete(TEntity obj)
        {
            db.Set<TEntity>().Remove(obj);
        }

        public void Update(TEntity obj)
        {
            db.Entry(obj).State = EntityState.Modified;
        }
    }
}
