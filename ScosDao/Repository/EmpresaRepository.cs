﻿using Scos.DAL.Repository.Base;
using System.Collections.Generic;
using System.Linq;
using Scos.Entities;
using System;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Scos.DAL.Repository
{
    public class EmpresaRepository : Repository<tblEmpresa>, IEmpresaRepository
    {
        // construtor para setar o contexto na classe base
        public EmpresaRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public int CadastrarComoCliente(Empresa cliente)
        {
            try
            {
                // novo cliente
                var novoCliente = Add(new tblEmpresa()
                {
                    Id = cliente.id,
                    Ativo = true,
                    Bairro = cliente.bairro.Trim(),
                    Cep = cliente.cep.Trim(),
                    CidadeId = cliente.cidade.id,
                    Cnpj = cliente.cnpj.Trim(),
                    Complemento = cliente.complemento.Trim(),
                    Endereco = cliente.endereco.Trim(),
                    EstadoId = cliente.estado.id,
                    NomeFantasia = cliente.nomeFantasia.Trim(),
                    Numero = cliente.numero.Trim(),
                    RazaoSocial = cliente.razaoSocial.Trim(),
                    TelContato = cliente.telContato.Trim(),
                    Municipio = cliente.municipio.Trim()
                });

                // empresa prestadora
                var empresa = Find(e => e.Id == cliente.usuarioInteracao.empresa.id);

                ctx.SaveChanges();

                // cliente -> prestador de serviço
                new Repository<tblClienteEmpresa>(ctx).Add(new tblClienteEmpresa()
                {
                    ClienteId = novoCliente.Id,
                    EmpresaId = empresa.Id
                });

                return novoCliente.Id;
            }
            //catch (Exception ex)
            //{
            //    return 0;
            //    throw;
            //}
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Debug.WriteLine("Erro ==================================================");
                    Debug.WriteLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Debug.WriteLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                    Debug.WriteLine("======================================================");
                }
                return 0;
                throw;
            }
        }

        public Empresa Cliente(int empresaId)
        {
            try
            {
                var cliente = (from emp in ctx.tblEmpresa
                               join cid in ctx.tblCidade on emp.CidadeId equals cid.Id
                               join uf in ctx.tblEstado on emp.EstadoId equals uf.Id
                               where emp.Id == empresaId && emp.Ativo == true
                               select new Empresa
                               {
                                   id = emp.Id,
                                   ativo = emp.Ativo,
                                   bairro = emp.Bairro,
                                   cep = emp.Cep,
                                   cidade = new Cidade
                                   {
                                       id = emp.CidadeId,
                                       nomeCidade = cid.Cidade
                                   },
                                   cnpj = emp.Cnpj,
                                   complemento = emp.Complemento,
                                   endereco = emp.Endereco,
                                   nomeFantasia = emp.NomeFantasia,
                                   numero = emp.Numero,
                                   razaoSocial = emp.RazaoSocial,
                                   telContato = emp.TelContato,
                                   estado = new Estado
                                   {
                                       id = uf.Id,
                                       nomeEstado = uf.Estado,
                                       sigla = uf.Sigla
                                   }
                               }).FirstOrDefault();

                return cliente;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<Empresa> Clientes(int empresaId)
        {
            try
            {
                var listEmpresa = (from e in ctx.tblEmpresa
                                   join ce in ctx.tblClienteEmpresa on e.Id equals ce.ClienteId
                                   join uf in ctx.tblEstado on e.EstadoId equals uf.Id
                                   join cid in ctx.tblCidade on e.CidadeId equals cid.Id
                                   where empresaId == 0  || ce.EmpresaId == empresaId && e.Ativo == true
                                   select new Empresa()
                                   {
                                       ativo = e.Ativo,
                                       bairro = e.Bairro.Trim(),
                                       cep = e.Cep.Trim(),
                                       cidade = new Cidade
                                       {
                                           id = cid.Id,
                                           nomeCidade = cid.Cidade.Trim()
                                       },
                                       cnpj = e.Cnpj.Trim(),
                                       complemento = e.Complemento.Trim(),
                                       endereco = e.Endereco.Trim(),
                                       estado = new Estado
                                       {
                                           id = uf.Id,
                                           nomeEstado = uf.Estado.Trim(),
                                           sigla = uf.Sigla.Trim()
                                       },
                                       nomeFantasia = e.NomeFantasia.Trim(),
                                       numero = e.Numero.Trim(),
                                       razaoSocial = e.RazaoSocial.Trim(),
                                       telContato = e.TelContato.Trim(),
                                       id = e.Id
                                   }).ToList();

                return listEmpresa;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int Desativar(int id)
        {
            try
            {
                var empresa = Get(id);
                empresa.Ativo = false;
                Update(empresa);

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int Editar(Empresa empresa)
        {
            try
            {
                Update(new tblEmpresa
                {
                    Ativo = empresa.ativo,
                    Bairro = empresa.bairro,
                    Cep = empresa.cep,
                    CidadeId = empresa.cidade.id,
                    Cnpj = empresa.cnpj,
                    Complemento = empresa.complemento,
                    Endereco = empresa.endereco,
                    EstadoId = empresa.estado.id,
                    Id = empresa.id,
                    NomeFantasia = empresa.nomeFantasia,
                    Numero = empresa.numero,
                    RazaoSocial = empresa.razaoSocial,
                    TelContato = empresa.telContato,
                    Municipio = empresa.municipio                    
                });
                
                ctx.SaveChanges();

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
            //catch (DbEntityValidationException e)
            //{
            //    foreach (var eve in e.EntityValidationErrors)
            //    {
            //        Debug.WriteLine("Erro ==================================================");
            //        Debug.WriteLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State));
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Debug.WriteLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage));
            //        }
            //        Debug.WriteLine("======================================================");
            //    }
            //    throw;
            //}
        }

        public List<Empresa> Empresas()
        {
            try
            {
                var tempListEmpresa = GetAll();
                var listEmpresa = new List<Empresa>();

                foreach (var emp in tempListEmpresa)
                {
                    listEmpresa.Add(new Empresa() {
                        id = emp.Id,
                        nomeFantasia = emp.NomeFantasia.Trim(),
                        endereco = emp.Endereco.Trim(),
                        municipio = emp.Municipio.Trim(),
                        bairro = emp.Bairro.Trim(),
                        cidade = new Cidade
                        {
                            id = emp.CidadeId,
                            nomeCidade = new Repository<tblCidade>(ctx).Get(emp.CidadeId).Cidade.Trim()
                        },
                        cnpj = emp.Cnpj.Trim(),
                        numero = emp.Numero.Trim(),
                        complemento = emp.Complemento.Trim(),
                        telContato = emp.TelContato.Trim(),
                        cep = emp.Cep.Trim(),
                        ativo = emp.Ativo,
                        razaoSocial = emp.RazaoSocial,
                        estado = new Estado
                        {
                            id = emp.EstadoId,
                            nomeEstado = new Repository<tblEstado>(ctx).Get(emp.EstadoId).Estado.Trim(),
                            sigla = new Repository<tblEstado>(ctx).Get(emp.EstadoId).Sigla.Trim(),
                        }
                    });
                }

                return listEmpresa;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
