﻿using Scos.DAL;
using Scos.DAL.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;

namespace Scos.DAL.Repository
{
    public interface IEmpresaRepository : IRepository<tblEmpresa>
    {
        int CadastrarComoCliente(Empresa cliente);
        List<Empresa> Clientes(int empresaId);
        List<Empresa> Empresas();
        Empresa Cliente(int empresaId);
        int Editar(Empresa empresa);
        int Desativar(int id);
    }
}
