﻿using Scos.DAL.Context;
using Scos.DAL.Repository.Base;

namespace Scos.DAL.Repository
{
    public interface ICidadeRepository : IRepository<tblCidade> 
    {
    }
}
