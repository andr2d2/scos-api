﻿using Scos.DAL.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;

namespace Scos.DAL.Repository
{
    public interface IGeografiaRepository : IRepository<tblEstado>
    {
        List<Estado> Estados();
        List<Cidade> Cidades(int id);
    }
}