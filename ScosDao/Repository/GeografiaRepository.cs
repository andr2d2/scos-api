﻿using System;
using Scos.DAL;
using Scos.DAL.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Scos.DAL.Repository
{
    public class GeografiaRepository : Repository<tblEstado>, IGeografiaRepository
    {
        // construtor para setar o contexto na classe base
        public GeografiaRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public List<Estado> Estados()
        {
            try
            {
                var listEstados = (from uf in ctx.tblEstado
                                   select new Estado
                                   {
                                       id = uf.Id,
                                       nomeEstado = uf.Estado.Trim(),
                                       sigla = uf.Sigla.Trim()
                                   }).ToList();

                return listEstados;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<Cidade> Cidades(int id)
        {
            try
            {
                var listCidades = (from c in ctx.tblCidade
                                   where c.EstadoId == id
                                   select new Cidade
                                   {
                                       id = c.Id,
                                       nomeCidade = c.Cidade.Trim()
                                   }).ToList();

                return listCidades;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
