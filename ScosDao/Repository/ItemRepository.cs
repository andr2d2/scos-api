﻿using Scos.DAL.Repository.Base;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scos.DAL.Repository
{
    public class ItemRepository : Repository<tblItem>, IItemRepository
    {
        // construtor para setar o contexto na classe base
        public ItemRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public Item Adicionar(Item item)
        {
            try
            {
                // verificar se o chamado esta diferente aberto
                //var statusChamado = new Repository<tblChamado>(ctx).Find(h => h.Id == item.chamadoId);

                //if (statusChamado != null && statusChamado.Id != 1)
                //    return new Item();

                // verifica se item ja existe vinculado ao cliente
                var itemExistente = new Repository<tblItem>(ctx).Find(i => i.NumeroSerie == item.numeroSerie);

                // usa item existente
                if (itemExistente != null)
                {
                    item.id = itemExistente.Id;
                }
                else
                {
                    var itemAdicionado = Add(new tblItem
                    {
                        NumeroSerie = item.numeroSerie.Trim(),
                        //Descricao = item.descricao.Trim(),
                    });

                    item.id = itemAdicionado.Id;
                }

                // verifiaca se já não esta adicionado
                var itemAdicionadoChamado = new Repository<tblItemChamado>(ctx).Find(ic => ic.ItemId == item.id);

                // adicionar item ao chamado
                if (itemAdicionadoChamado == null)
                {
                    new Repository<tblItemChamado>(ctx).Add(new tblItemChamado()
                    {
                        //ChamadoId = item.chamadoId,
                        ItemId = item.id
                    });
                }


                ctx.SaveChanges();

                return item;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public int Cadastrar(Item item)
        {
            try
            {
                Add(new tblItem {
                    Equipamento = item.equipamento.Trim(),
                    Id = item.id,
                    Modelo = item.modelo.Trim(),
                    NumeroSerie = item.numeroSerie.Trim()                    
                });

                return ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public void Excluir(Item item)
        {
            try
            {
                new Repository<tblItemChamado>(ctx).Delete(new tblItemChamado
                {
                    ItemId = item.id
                });

                ctx.SaveChanges();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<Item> Itens()
        {
            try
            {
                var listItens = new List<Item>();
                var itens = GetAll().ToList();

                foreach (var item in itens)
                {
                    listItens.Add(new Item {
                        equipamento = item.Equipamento.Trim(),
                        id = item.Id,
                        numeroSerie = item.NumeroSerie.Trim(),
                        modelo = item.Modelo.Trim()
                    });
                }

                return listItens;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
