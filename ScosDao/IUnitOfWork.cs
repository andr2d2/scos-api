﻿using Scos.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scos.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IChamadoRepository Chamado { get; }
        IGeografiaRepository Geografia { get; }
        IEmpresaRepository Empresa { get; }
        IUsuarioRepository Usuario { get; }
        int Commit();
    }
}
