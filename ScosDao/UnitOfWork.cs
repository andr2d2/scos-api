﻿using Scos.DAL.Context;
using Scos.DAL.Repository;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Scos.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private  scosEntities _context;

        public UnitOfWork()
        {
            _context = new scosEntities();
            Geografia = new GeografiaRepository(_context);
            Empresa = new EmpresaRepository(_context);
            Chamado = new ChamadoRepository(_context);
            Usuario = new UsuarioRepository(_context);
        }

        public IGeografiaRepository Geografia { get; }
        public IEmpresaRepository Empresa { get; }
        public IChamadoRepository Chamado { get; }
        public IUsuarioRepository Usuario { get; }



        public int Commit()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Debug.WriteLine("Erro ==================================================");
                    Debug.WriteLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Debug.WriteLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                    Debug.WriteLine("======================================================");
                }
                throw;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
