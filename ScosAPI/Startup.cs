﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(ScosAPI.Startup))]

namespace ScosAPI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                    // EnableJSONP = true
                };
                map.RunSignalR(hubConfiguration);
            });

            //var config = new HubConfiguration();
            //config.EnableJSONP = true;
            //app.MapSignalR(config);
        }
    }
}
