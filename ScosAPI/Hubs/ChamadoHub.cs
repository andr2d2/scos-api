﻿using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;
using Scos.Entities;
using Scos.DAO.Repository.Base;
using Dao.Context;
using Scos.DAL.Repository;
using System;
using System.Collections.Generic;
using Model;

namespace ScosAPI.Hubs
{
    [HubName("ChamadoHub")]
    public class ChamadoHub : Hub
    {
        public List<Chamado> GetChamados(int userId)
        {
            try
            {
                using (var ctx = new scosEntities())
                {                    
                    var lisChamados = new ChamadoRepository(ctx).ChamadosPorUsuario(userId);
                    return lisChamados;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<ChamadoMap> GetChamadosMap()
        {
            try
            {
                using (var ctx = new scosEntities())
                {
                    var pins = new ChamadoRepository(ctx).PinChamados();
                    return pins;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<TaskStatus> CadastrarChamado(Chamado chamado)
        {
            try
            {
                using (var ctx = new scosEntities())
                {
                    var novoChamado = new ChamadoRepository(ctx).Cadastrar(chamado);
                    var pin = new ChamadoRepository(ctx).PinChamado(novoChamado.id);

                    await Clients.Caller.novoChamadoUsuario(novoChamado);
                    Clients.All.novoChamadoMap(pin);

                    return TaskStatus.Running;
                }
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public async Task<TaskStatus> EditarChamado(Chamado chamado)
        {
            try
            {
                using (var ctx = new scosEntities())
                {
                    var chamadoEditado = new ChamadoRepository(ctx).Editar(chamado);

                    await Clients.Caller.editarChamadoUsuario(chamadoEditado);
                    //Clients.All.novoChamadoMap(pin);

                    return TaskStatus.Running;
                }
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int Atender(Chamado chamado)
        {
            try
            {
                using (var ctx = new scosEntities())
                {
                   
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }
    }
}
