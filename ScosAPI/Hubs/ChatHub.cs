﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;
using Scos.Entities;
using Scos.DAO.Repository.Base;
using Dao.Context;

namespace ScosAPI.Hubs
{
    [HubName("ChatHub")]
    public class ChatHub : Hub
    {
        public override Task OnConnected()
        {
            var userId = Convert.ToInt32(Context.QueryString["userId"]);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {   
            return base.OnDisconnected(stopCalled);
        }

        public void SendMsg(string remetente, string msg)
        {
            try
            {
                //IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();

                //context.
                Clients.All.msgAdded(string.Format("{0} ({1})", remetente, DateTime.Now.ToLocalTime()), msg);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task Join(Usuario user)
        {
            try
            {
                var nomeGrupo = user.empresa.nomeFantasia;
                await Groups.Add(Context.ConnectionId, nomeGrupo);
                Clients.Group(nomeGrupo).userJoin(string.Format("{0} ({1}) acabou de entrar", user.nomeCompleto, DateTime.Now.ToLocalTime()));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task Leave(Usuario user)
        {
            try
            {
                var nomeGrupo = user.empresa.nomeFantasia;
                await Groups.Remove(Context.ConnectionId, nomeGrupo);
                await Clients.Group(nomeGrupo).userLeave(string.Format("{0} ({1}) acabou de sair", user.nomeCompleto, DateTime.Now.ToLocalTime()));
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}