﻿using Scos.DAL;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ScosApi.Controllers
{
    [RoutePrefix("api/Item")]
    public class ItemController : ApiController
    {
        [AcceptVerbs("GET")]
        [ResponseType(typeof(List<Item>))]
        public HttpResponseMessage ListarItens()
        {
            var uow = new UnitOfWork();
            try
            {
                var itens = uow.Item.Itens();
                return Request.CreateResponse(HttpStatusCode.OK, itens);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("GET")]
        [Route("Item/{id:int}")]
        [ResponseType(typeof(List<Item>))]
        public HttpResponseMessage Item(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var itens = uow.Item.Itens().Find(x => x.id == id);
                return Request.CreateResponse(HttpStatusCode.OK, itens);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("POST")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Cadastrar([FromBody] Item item)
        {
            var uow = new UnitOfWork();
            try
            {
                var itemCriada = uow.Item.Cadastrar(item);
                if (itemCriada != 0)
                    return Request.CreateResponse(HttpStatusCode.Created, itemCriada);
                else
                    return Request.CreateResponse(HttpStatusCode.NoContent, itemCriada);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }
    }
}
