﻿using Scos.DAL;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ScosApi.Controllers
{
    [RoutePrefix("api/Geo")]
    public class GeografiaController : ApiController
    {
        [AcceptVerbs("GET")]
        [Route("Estados")]
        [ResponseType(typeof(List<Estado>))]
        public HttpResponseMessage Estados()
        {
            var uow = new UnitOfWork();
            try
            {
                var estados = uow.Geografia.Estados().OrderBy(uf => uf.nomeEstado);
                return Request.CreateResponse(HttpStatusCode.OK, estados);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("GET")]
        [Route("Estado/{id:int}")]
        [ResponseType(typeof(Estado))]
        public HttpResponseMessage Estado(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var estado = uow.Geografia.Estados().Where(uf => uf.id == id).FirstOrDefault();
                return Request.CreateResponse(HttpStatusCode.OK, estado);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("GET")]
        [Route("Estado/{id:int}/Cidades/")]
        [ResponseType(typeof(List<Cidade>))]
        public HttpResponseMessage Cidades(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var cidades = uow.Geografia.Cidades(id).OrderBy(c => c.nomeCidade);
                return Request.CreateResponse(HttpStatusCode.OK, cidades);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("GET")]
        [Route("Cidade/{id:int}")]
        [ResponseType(typeof(Cidade))]
        public HttpResponseMessage Cidade(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var estado = uow.Geografia.Cidades(id);
                return Request.CreateResponse(HttpStatusCode.OK, estado);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }
    }
}
