﻿using Newtonsoft.Json;
using Scos.DAL;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ScosApi.Controllers
{
    [RoutePrefix("api/OrdemServico")]
    public class OrdemServicoController : ApiController
    {
        /// <summary>
        /// Cadastra uma ordem de serviço
        /// </summary>
        /// <param name="os"></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Cadastrar(OrdemServico os)
        {
            var uow = new UnitOfWork();
            try
            {
                var retornoOs = uow.OrdemServico.Cadastrar(os);
                return Request.CreateResponse(HttpStatusCode.Created, retornoOs);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Adiciona inicio de análise a uma ordem de serviço
        /// </summary>
        /// <param name="os">OrdemServico</param>
        /// <returns>1</returns>
        [AcceptVerbs("PUT")]
        [Route("InicioAnalise")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage InicioAnalise(OrdemServico os)
        {
            var uow = new UnitOfWork();
            try
            {
                var retornoOs = uow.OrdemServico.InicioAnalise(os);

                if (retornoOs == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, retornoOs);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Adiciona fim de análise a uma ordem de serviço
        /// </summary>
        /// <param name="os">OrdemServico</param>
        /// <returns>1</returns>
        [AcceptVerbs("PUT")]
        [Route("FimAnalise")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage FimAnalise(OrdemServico os)
        {
            var uow = new UnitOfWork();
            try
            {
                var retornoOs = uow.OrdemServico.FimAnalise(os);

                if (retornoOs == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, retornoOs);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Adiciona informações de conserto
        /// </summary>
        /// <param name="os">OrdemServico</param>
        /// <returns>1</returns>
        [AcceptVerbs("PUT")]
        [Route("Conserto")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Conserto(OrdemServico os)
        {
            var uow = new UnitOfWork();
            try
            {
                var retornoOs = uow.OrdemServico.Conserto(os);

                if (retornoOs == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, retornoOs);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Autoriza uma ordem de serviço
        /// </summary>
        /// <param name="os">OrdemServico</param>
        /// <returns>1</returns>
        [AcceptVerbs("PUT")]
        [Route("Autorizacao")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Autorizacao(OrdemServico os)
        {
            var uow = new UnitOfWork();
            try
            {
                var retornoOs = uow.OrdemServico.Autorizacao(os);

                if (retornoOs == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, retornoOs);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Encerra uma ordem de serviço
        /// </summary>
        /// <param name="id">Id da ordem de serviço</param>
        /// <returns>1</returns>
        [AcceptVerbs("POST")]
        [Route("Fechar")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Fechar([FromBody]int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var retornoOs = uow.OrdemServico.Fechar(id);

                if (retornoOs == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, retornoOs);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Lista ordens de serviço
        /// </summary>
        /// <returns>List OrdemServico</returns>
        [AcceptVerbs("GET")]
        [ResponseType(typeof(List<OrdemServico>))]
        public HttpResponseMessage ListarOS()
        {
            var uow = new UnitOfWork();
            try
            {
                var clientes = uow.OrdemServico.Listar();
                return Request.CreateResponse(HttpStatusCode.OK, clientes);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Lista as ordens de serviço de um usuário
        /// </summary>
        /// <param name="id">Id usuário</param>
        /// <returns>List OrdemServico</returns>
        [AcceptVerbs("GET")]
        [Route("Usuario/{id:int}")]
        [ResponseType(typeof(List<OrdemServico>))]
        public HttpResponseMessage ListarOsUsuario(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var clientes = uow.OrdemServico.Listar().FindAll(os => os.usuario.id == id);
                return Request.CreateResponse(HttpStatusCode.OK, clientes);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Retorna uma ordem de serviço específica
        /// </summary>
        /// <param name="id">Id ordem de serviço</param>
        /// <returns>OrdemServico</returns>
        [AcceptVerbs("GET")]
        [Route("{id:int}")]
        [ResponseType(typeof(OrdemServico))]
        public HttpResponseMessage GetOS(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var retorno = uow.OrdemServico.GetOs(id);
                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Adiciona o deslocamento de uma ordem de serviço
        /// </summary>
        /// <param name="os">OrdemServico</param>
        /// <returns>1</returns>
        [AcceptVerbs("PUT")]
        [Route("Deslocamento")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Deslocamento(OrdemServico os)
        {
            var uow = new UnitOfWork();
            try
            {
                var retornoOs = uow.OrdemServico.Deslocamento(os);

                if (retornoOs == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, retornoOs);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Editar uma ordem de serviço
        /// </summary>
        /// <param name="os"></param>
        /// <returns></returns>
        [AcceptVerbs("PUT")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Editar(OrdemServico os)
        {
            var uow = new UnitOfWork();
            try
            {
                var retornoOs = uow.OrdemServico.Editar(os);

                if (retornoOs == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, retornoOs);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }
    }
}
