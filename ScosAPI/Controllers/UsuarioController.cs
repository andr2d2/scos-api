﻿using Scos.DAL;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ScosApi.Controllers
{
    /// <summary>
    /// Controller Usuário
    /// </summary>
    [RoutePrefix("api/Usuario")]
    public class UsuarioController : ApiController
    {
        /// <summary>
        /// Retorna os tipos de usuário
        /// </summary>
        /// <returns>List TipoUsuario</returns>
        [AcceptVerbs("GET")]
        [Route("TipoUsuario")]
        [ResponseType(typeof(List<TipoUsuario>))]
        public HttpResponseMessage TiposUsuario()
        {
            var uow = new UnitOfWork();
            try
            {
                var tiposUsuarios = uow.Usuario.TiposUsuario();
                return Request.CreateResponse(HttpStatusCode.OK, tiposUsuarios);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Retorna um tipo de usuário específico
        /// </summary>
        /// <param name="id">Tipo Id</param>
        /// <returns>TipoUsuario</returns>
        [AcceptVerbs("GET")]
        [Route("TipoUsuario/{id:int}")]
        [ResponseType(typeof(TipoUsuario))]
        public HttpResponseMessage TipoUsuario(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var tiposUsuario = uow.Usuario.TipoUsuario(id);
                return Request.CreateResponse(HttpStatusCode.OK, tiposUsuario);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Cadastra um novo usuário
        /// </summary>
        /// <param name="usuario">Objeto Usuario</param>
        /// <returns>Id do usuário cadastrado</returns>
        [AcceptVerbs("POST")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Cadastrar([FromBody] Usuario usuario)
        {
            var uow = new UnitOfWork();
            try
            {
                var usuarioCadastrado = uow.Usuario.Cadastrar(usuario);

                if (usuarioCadastrado == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.Created, usuarioCadastrado);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Desativa um usuario (soft delete)
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>1</returns>
        [AcceptVerbs("PUT")]
        [Route("Desativar/{id:int}")]
        public HttpResponseMessage Desativar([FromBody] int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var usuarioDesativado = uow.Usuario.Desativar(id);

                if (usuarioDesativado == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, usuarioDesativado);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Edita um usuário
        /// </summary>
        /// <param name="usuario">Objeto Usuario</param>
        /// <returns>1</returns>
        [AcceptVerbs("PUT")]
        [Route("Editar")]
        [ResponseType(typeof(Usuario))]
        public HttpResponseMessage Editar([FromBody] Usuario usuario)
        {
            var uow = new UnitOfWork();
            try
            {
                var usuarioEditada = uow.Usuario.Editar(usuario);

                if (usuarioEditada == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, usuarioEditada);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Lista os usuários de uma empresa
        /// </summary>
        /// <param name="empresaId">Id da empresa</param>
        /// <returns>List Usuario</returns>
        [AcceptVerbs("GET")]
        [Route("Empresa/{empresaId:int}")]
        [ResponseType(typeof(List<Usuario>))]
        public HttpResponseMessage Usuarios(int empresaId)
        {
            var uow = new UnitOfWork();
            try
            {
                var usuarios = uow.Usuario.Usuarios(empresaId);
                return Request.CreateResponse(HttpStatusCode.OK, usuarios);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Retorna um usuário através do Id
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Usuario</returns>
        [AcceptVerbs("GET")]
        [Route("{id:int}")]
        [ResponseType(typeof(Usuario))]
        public HttpResponseMessage Usuario(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var usuario = uow.Usuario.Usuario(id);
                return Request.CreateResponse(HttpStatusCode.OK, usuario);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Login de acesso
        /// </summary>
        /// <param name="acesso">Objeto Acesso</param>
        /// <returns>Usuario</returns>
        [AcceptVerbs("POST")]
        [Route("Login")]
        [ResponseType(typeof(Usuario))]
        public HttpResponseMessage Login([FromBody] Acesso acesso)
        {
            var uow = new UnitOfWork();
            try
            {
                var logado = uow.Usuario.Login(acesso);

                if (logado == null)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, logado);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Reset de senha
        /// </summary>
        /// <param name="usuario">Objeto Usuario</param>
        /// <returns>Usuario</returns>
        [AcceptVerbs("POST")]
        [Route("ResetSenha")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage ResetSenha([FromBody] int usuarioId)
        {
            var uow = new UnitOfWork();
            try
            {
                var reset = uow.Usuario.ResetSenha(usuarioId);

                if (reset == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, reset);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Primeiro acesso
        /// </summary>
        /// <param name="acesso">Objeto Acesso</param>
        /// <returns>Usuario</returns>
        [AcceptVerbs("POST")]
        [Route("PrimeiroAcesso")]
        [ResponseType(typeof(Usuario))]
        public HttpResponseMessage PrimeiroAcesso([FromBody] Acesso acesso)
        {
            var uow = new UnitOfWork();
            try
            {
                var logado = uow.Usuario.PrimeiroAcesso(acesso);

                if (logado == null)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, logado);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }
    }
}
