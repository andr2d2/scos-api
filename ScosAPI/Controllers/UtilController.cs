﻿using Scos.DAL;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ScosApi.Controllers
{
    /// <summary>
    /// Controller de utilidades
    /// </summary>
    [RoutePrefix("api/Util")]
    public class UtilController : ApiController
    {
        /// <summary>
        /// Lista os status
        /// </summary>
        /// <returns>List Status</returns>
        [AcceptVerbs("GET")]
        [Route("ListarStatus")]
        [ResponseType(typeof(List<Status>))]
        public HttpResponseMessage ListarStatus()
        {
            var uow = new UnitOfWork();
            try
            {
                var retorno = uow.Util.ListarStatus();
                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Lista as UF
        /// </summary>
        /// <returns>List Estado</returns>
        [AcceptVerbs("GET")]
        [Route("Estados")]
        [ResponseType(typeof(List<Estado>))]
        public HttpResponseMessage Estados()
        {
            var uow = new UnitOfWork();
            try
            {
                var estados = uow.Geografia.Estados().OrderBy(uf => uf.nomeEstado);

                return Request.CreateResponse(HttpStatusCode.OK, estados);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Retorna uma UF
        /// </summary>
        /// <param name="id">Id da UF</param>
        /// <returns>Estado</returns>
        [AcceptVerbs("GET")]
        [Route("Estado/{id:int}")]
        [ResponseType(typeof(Estado))]
        public HttpResponseMessage Estado(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var estado = uow.Geografia.Estados().Where(uf => uf.id == id).FirstOrDefault();

                return Request.CreateResponse(HttpStatusCode.OK, estado);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Retorna as cidades de um estado
        /// </summary>
        /// <param name="id">Id da UF</param>
        /// <returns>List Cidade</returns>
        [AcceptVerbs("GET")]
        [Route("Estado/{id:int}/Cidades/")]
        [ResponseType(typeof(List<Cidade>))]
        public HttpResponseMessage Cidades(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var cidades = uow.Geografia.Cidades(id).OrderBy(c => c.nomeCidade);

                return Request.CreateResponse(HttpStatusCode.OK, cidades);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Retorna uma cidade
        /// </summary>
        /// <param name="id">Id da cidade</param>
        /// <returns>Cidade</returns>
        [AcceptVerbs("GET")]
        [Route("Cidade/{id:int}")]
        [ResponseType(typeof(Cidade))]
        public HttpResponseMessage Cidade(int id)
        {
            var uow = new UnitOfWork();
            try
            {
                var estado = uow.Geografia.Cidades(id);

                return Request.CreateResponse(HttpStatusCode.OK, estado);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Retorna os tipos de ordem de serviço
        /// </summary>
        /// <returns>List TipoOrdemServico</returns>
        [AcceptVerbs("GET")]
        [Route("TipoOrdemServico")]
        [ResponseType(typeof(List<TipoOrdemServico>))]
        public HttpResponseMessage ListarTipos()
        {
            var uow = new UnitOfWork();
            try
            {
                var retorno = uow.Util.ListarTipoOs();
                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

    }
}
