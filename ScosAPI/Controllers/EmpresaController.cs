﻿using Scos.DAL;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ScosApi.Controllers
{
    /// <summary>
    /// Controller empresa
    /// </summary>
    [RoutePrefix("api/Empresa")]
    public class EmpresaController : ApiController
    {
        /// <summary>
        /// Cadastrar uma nova empresa, que é vinculada como cliente da empresa do usuário atual
        /// </summary>
        /// <param name="empresa">Empresa</param>
        /// <returns>Id da empresa cadastrada</returns>
        [AcceptVerbs("POST")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Cadastrar([FromBody] Empresa empresa)
        {
            var uow = new UnitOfWork();
            try
            {
                var empresaCriada = uow.Empresa.Cadastrar(empresa);                
               return Request.CreateResponse(HttpStatusCode.Created, empresaCriada);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,  ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Desativa uma empresa (soft delete)
        /// </summary>
        /// <param name="id">Id da empresa</param>
        /// <returns>1</returns>
        [AcceptVerbs("PUT")]
        [Route("Cliente/Desativar")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Desativar([FromBody] int id)
        {
            var uow = new UnitOfWork();
            try
            {
                uow.Empresa.Desativar(id);
                

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,  ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Edita uma empresa cadastrada
        /// </summary>
        /// <param name="empresa">Empresa</param>
        /// <returns>1</returns>
        [AcceptVerbs("PUT")]
        [Route("Cliente/Editar")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Editar([FromBody] Empresa empresa)
        {
            var uow = new UnitOfWork();
            try
            {
                var empresaEditada = uow.Empresa.Editar(empresa);

                if (empresaEditada == 0)
                    throw new Exception();

                    return Request.CreateResponse(HttpStatusCode.OK, empresaEditada);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,  ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Lista as empresas clientes ativas da empresa infomada
        /// </summary>
        /// <param name="empresaId">Id da empresa</param>
        /// <returns>(List Empresa</returns>
        [AcceptVerbs("GET")]
        [Route("{empresaId:int}/Clientes")]
        [ResponseType(typeof(List<Empresa>))]
        public HttpResponseMessage ListarClientes(int empresaId)
        {
            var uow = new UnitOfWork();
            try
            {
                var clientes = uow.Empresa.Clientes(empresaId);
                

                return Request.CreateResponse(HttpStatusCode.OK, clientes);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,  ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Retorna uma empresa específica
        /// </summary>
        /// <param name="empresaId">Id da empresa</param>
        /// <returns>Empresa</returns>
        [AcceptVerbs("GET")]
        [Route("{empresaId:int}")]
        [ResponseType(typeof(Empresa))]
        public HttpResponseMessage Cliente(int empresaId)
        {
            var uow = new UnitOfWork();
            try
            {
                var cliente = uow.Empresa.Cliente(empresaId);
                

                return Request.CreateResponse(HttpStatusCode.OK, cliente);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,  ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Retorna todas as empresas
        /// </summary>
        /// <returns>List Empresa</returns>
        [AcceptVerbs("GET")]
        [ResponseType(typeof(List<Empresa>))]
        public HttpResponseMessage ListarEmpresas()
        {
            var uow = new UnitOfWork();
            try
            {
                var clientes = uow.Empresa.Empresas();
                return Request.CreateResponse(HttpStatusCode.OK, clientes);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,  ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        /// <summary>
        /// Lista as empresas clientes inativos da empresa infomada
        /// </summary>
        /// <param name="empresaId">Id da empresa</param>
        /// <returns>(List Empresa</returns>
        [AcceptVerbs("GET")]
        [Route("{empresaId:int}/ClientesInativos")]
        [ResponseType(typeof(List<Empresa>))]
        public HttpResponseMessage ListarClientesInativos(int empresaId)
        {
            var uow = new UnitOfWork();
            try
            {
                var clientes = uow.Empresa.Clientes(empresaId, false);
                

                return Request.CreateResponse(HttpStatusCode.OK, clientes);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }
    }
}
