﻿using Scos.DAL;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ScosApi.Controllers
{
    /// <summary>
    /// Controller chamado
    /// </summary>
    [RoutePrefix("api/Chamado")]
    public class ChamadoController : ApiController
    {
        /// <summary>
        /// Cadastra um novo chamado
        /// </summary>
        /// <param name="chamado">Chamado</param>
        /// <returns>Id do chamado criado</returns>
        [AcceptVerbs("POST")]
        [ResponseType(typeof(Chamado))]
        public HttpResponseMessage Cadastrar([FromBody]Chamado chamado)
        {
            var uow = new UnitOfWork();
            try
            {
                var retorno = uow.Chamado.Cadastrar(chamado);

                if (retorno == null)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.Created, retorno);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("PUT")]
        [Route("Editar")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Editar([FromBody]Chamado chamado)
        {
            var uow = new UnitOfWork();
            try
            {
                //só pode alterar status do chamado aberto
                if (chamado.status.id != (int)Status.enumStatus.Aberto)
                    throw new Exception();

                var retorno = uow.Chamado.Editar(chamado);
                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("POST")]
        [Route("Fechar")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage Fechar([FromBody]ChamadoUsuario chamadoUsuario)
        {
            var uow = new UnitOfWork();
            try
            {
                var retorno = uow.Chamado.Fechar(chamadoUsuario);

                if (retorno == 0)
                    throw new Exception();

                return Request.CreateResponse(HttpStatusCode.OK, retorno);                    
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(List<Chamado>))]
        public HttpResponseMessage Chamados()
        {
            var uow = new UnitOfWork();
            try
            {
                var chamados = uow.Chamado.Chamados();
                return Request.CreateResponse(HttpStatusCode.OK, chamados);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("GET")]
        [Route("Empresa/{empresaId:int}")]
        [ResponseType(typeof(List<Chamado>))]
        public HttpResponseMessage ChamadosPorEmpresa(int empresaId)
        {
            var uow = new UnitOfWork();
            try
            {
                var chamados = uow.Chamado.ChamadosPorEmpresa(empresaId);
                return Request.CreateResponse(HttpStatusCode.OK, chamados);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

        [AcceptVerbs("GET")]
        [Route("Usuario/{usuarioId:int}")]
        [ResponseType(typeof(List<Chamado>))]
        public HttpResponseMessage ChamadosPorUsuario(int usuarioId)
        {
            var uow = new UnitOfWork();
            try
            {
                var chamados = uow.Chamado.ChamadosPorUsuario(usuarioId);
                return Request.CreateResponse(HttpStatusCode.OK, chamados);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }

    }
}
