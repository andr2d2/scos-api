﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scos.Entities;
using ScosApi.Controllers;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ScosApi.Tests
{
    [TestClass]
    public class EmpresaTest
    {
        [TestMethod]
        public void Post_Cadastrar_Cliente_Empresa()
        {
            var ctrl = new EmpresaController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var cliente = new Empresa
            {
                ativo = true,
                bairro = "tBairro",
                cep = "5555",
                cidade = new Cidade
                {
                    id = 3304557
                },
                cnpj = "987987987",
                complemento = "Complemento",
                endereco = "Endereco",
                nomeFantasia = "NomeFantasia",
                numero = "3333",
                razaoSocial = "22222",
                telContato = "11111",
                estado = new Estado
                {
                    id = 33
                },
                usuarioInteracao = new Usuario
                {
                    id = 1,
                    empresa = new Empresa
                    {
                        id = 1
                    }
                }
            };

            var resp = ctrl.Cadastrar(cliente);
            var clienteCriado = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.Created, resp.StatusCode);
        }

        [TestMethod]
        public void Get_Clientes_Empresa()
        {
            var ctrl = new EmpresaController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.ListarClientes(1);
            var clientes = resp.Content.ReadAsAsync<List<Empresa>>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.IsInstanceOfType(clientes[0], typeof(Empresa));
        }

        [TestMethod]
        public void Get_Cliente_Empresa()
        {
            var ctrl = new EmpresaController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.Cliente(1);
            var cliente = resp.Content.ReadAsAsync<Empresa>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.IsInstanceOfType(cliente, typeof(Empresa));
        }

        [TestMethod]
        public void Put_Desativar_Cliente()
        {
            var ctrl = new EmpresaController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.Desativar(1);

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);            
        }

        [TestMethod]
        public void Put_Editar_Cliente()
        {
            var ctrl = new EmpresaController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var clienteEditado = new Empresa
            {
                id = 1,
                ativo = true,
                bairro = "tBairro",
                cep = "99999",
                cidade = new Cidade
                {
                    id = 3304557
                },
                cnpj = "99999",
                complemento = "Complemento",
                endereco = "Endereco",
                nomeFantasia = "NomeFantasia",
                numero = "9999",
                razaoSocial = "9999",
                telContato = "9999",
                estado = new Estado
                {
                    id = 33
                },
                usuarioInteracao = new Usuario
                {
                    id = 1,
                    empresa = new Empresa
                    {
                        id = 1
                    }
                }
            };

            var resp = ctrl.Editar(clienteEditado);
            var cliente = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.Created, resp.StatusCode);
            Assert.AreEqual(cliente, 1);
        }
    }
}
