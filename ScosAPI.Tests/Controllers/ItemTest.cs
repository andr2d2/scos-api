﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scos.Entities;
using ScosApi.Controllers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ScosApi.Tests
{
    [TestClass]
    public class ItemTest
    {

        [TestInitialize]
        public void TestInit()
        {
        }

        [TestMethod]
        public void Get_Itens()
        {
            var ctrl = new ItemController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.ListarItens();
            var retorno = resp.Content.ReadAsAsync<List<Item>>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.IsInstanceOfType(retorno[0], typeof(Item));
        }

        [TestMethod]
        public void Post_Itens()
        {
            var ctrl = new ItemController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var item = new Item {
                equipamento = "equip teste",
                modelo = "modelo teste",
                numeroSerie = "00000"
            };

            var resp = ctrl.Cadastrar(item);
            var retorno = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.Created, resp.StatusCode);
            Assert.IsTrue(retorno > 0);
        }
    }
}
