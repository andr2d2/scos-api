﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scos.Entities;
using ScosApi.Controllers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ScosApi.Tests
{
    [TestClass]
    public class ChamadoTest
    {
        public Usuario usuarioAtivo { get; set; }
        public Usuario usuarioInativo { get; set; }
        public Chamado chamadoNovo { get; set; }
        public Chamado chamadoCadastrado { get; set; }
        public List<Item> itens { get; set; }
        public Status statusAberto { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            usuarioAtivo = new Usuario
            {
                id = 1
            };

            usuarioInativo = new Usuario
            {
                id = 2
            };

            itens = new List<Item>();
            itens.Add(new Item
            {
                //descricao = "Teste item",
                numeroSerie = "123456ABC"
            });

            statusAberto = new Status
            {
                id = (int)Status.enumStatus.Aberto,
                status = Status.enumStatus.Aberto.ToString()
            };

            chamadoNovo = new Chamado
            {
                descricao = "Teste de chamado.",
                tipoChamado = new TipoChamado { id = (int)TipoChamado.enumTipo.Garantia },
                usuario = usuarioAtivo,
                ativo = true
            };

            chamadoCadastrado = new Chamado
            {
                id = 1,
                descricao = "Teste de chamado editado.",
                tipoChamado = new TipoChamado { id = (int)TipoChamado.enumTipo.Garantia },
                dataAbertura = new Util().DateTimeToUnix(DateTime.Now),
                usuario = usuarioAtivo,
                ativo = true
            };
        }

        [TestMethod]
        public void Get_Chamados()
        {
            var ctrl = new ChamadoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();
            
            var resp = ctrl.Chamados();
            var retorno = resp.Content.ReadAsAsync<List<Chamado>>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
        }

        [TestMethod]
        public void Get_Chamados_Por_Empresas()
        {
            var ctrl = new ChamadoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.ChamadosPorEmpresa(1);
            var retorno = resp.Content.ReadAsAsync<List<Chamado>>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
        }

        [TestMethod]
        public void Get_Chamados_Por_Usuario()
        {
            var ctrl = new ChamadoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.ChamadosPorUsuario(1);
            var retorno = resp.Content.ReadAsAsync<List<Chamado>>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
        }

        [TestMethod]
        public void Post_Cadastrar_Chamado_Adicionar_Item()
        {
            var ctrl = new ChamadoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var chamado = chamadoNovo;
            chamado.itens = itens;
            chamado.status = statusAberto;

            var resp = ctrl.Cadastrar(chamado);
            var retorno = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
        }

        [TestMethod]
        public void Post_Cadastrar_Chamado_Sem_Item()
        {
            var ctrl = new ChamadoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var chamado = chamadoNovo;
            chamado.status = statusAberto;

            var resp = ctrl.Cadastrar(chamado);
            var retorno = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
        }

        [TestMethod]
        public void Post_Cadastrar_Chamado_Sem_Item_Usuario_Inativo()
        {
            var ctrl = new ChamadoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var chamado = chamadoNovo;
            chamado.usuario = usuarioInativo;
            chamado.status = statusAberto;

            var resp = ctrl.Cadastrar(chamado);
            var retorno = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.NoContent, resp.StatusCode);
            Assert.AreEqual(retorno, 0);
        }

        [TestMethod]
        public void Put_Editar_Chamado_Sem_Item()
        {
            var ctrl = new ChamadoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var chamado = chamadoCadastrado;
            chamado.status = statusAberto;

            var resp = ctrl.Editar(chamado);
            var retorno = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.AreEqual(retorno, 1);
        }

        [TestMethod]
        public void Post_Fechar_Chamado()
        {
            var ctrl = new ChamadoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var chamadoUsuario = new ChamadoUsuario
            {
                chamadoId = 1,
                usuarioId = 1
            };

            var resp = ctrl.Fechar(chamadoUsuario);
            var retorno = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.AreEqual(retorno, 1);

        }      

    }
}
