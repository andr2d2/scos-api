﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scos.Entities;
using System;

namespace ScosApi.Tests
{
    [TestClass]
    public class UnixTimeTest
    {
        public DateTime dataTeste{ get; set; }

        [TestInitialize]
        public void testInit()
        {
            dataTeste = new DateTime(2016, 09, 07, 17, 53, 10);
        }

        [TestMethod]
        public void Get_DateTime()
        {
            var dateTime = new Util().UnixToDateTime(1473277990);
            Assert.AreEqual(dataTeste.ToString(), dateTime.ToString());
        }

        [TestMethod]
        public void Get_Unix()
        {
            var unix = new Util().DateTimeToUnix(dataTeste);
            Assert.AreEqual("1473277990", unix.ToString());
        }
    }
}
