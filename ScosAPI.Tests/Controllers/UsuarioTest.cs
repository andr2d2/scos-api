﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScosApi.Controllers;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using Scos.Entities;
using System.Collections.Generic;
using System;

namespace ScosApi.Tests
{
    [TestClass]
    public class UsuarioTest
    {
        public Usuario usuarioAtivo { get; set; }
        public Usuario usuarioInativo { get; set; }
        public Chamado chamadoNovo { get; set; }
        public Chamado chamadoCadastrado { get; set; }
        public List<Item> itens { get; set; }
        public Status statusAberto { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            usuarioAtivo = new Usuario
            {
                id = 1,
                cpf = "00000000000",
                nomeCompleto = "teste  user",
                email = "teste@teste",
                telContato = "33333333",
                empresa = new Empresa
                {
                    id = 1
                },
                tipousuario = new TipoUsuario
                {
                    id = 2
                },
                ativo = true
            };

            usuarioInativo = new Usuario
            {
                id = 2,
                cpf = "11111111111",
                nomeCompleto = "teste user",
                email = "teste@teste",
                telContato = "33333333",
                empresa = new Empresa
                {
                    id = 1
                },
                tipousuario = new TipoUsuario
                {
                    id = 2
                },
                ativo = false
            };

            itens = new List<Item>();
            itens.Add(new Item
            {
                //descricao = "Teste item",
                numeroSerie = "123456ABC",
            });

            statusAberto = new Status
            {
                id = (int)Status.enumStatus.Aberto,
                status = Status.enumStatus.Aberto.ToString()
            };

            chamadoNovo = new Chamado
            {
                descricao = "Teste de chamado.",
                tipoChamado = new TipoChamado { id = (int)TipoChamado.enumTipo.Garantia },
                usuario = usuarioAtivo,
                ativo = true
            };

            chamadoCadastrado = new Chamado
            {
                id = 1,
                descricao = "Teste de chamado editado.",
                tipoChamado = new TipoChamado { id = (int)TipoChamado.enumTipo.Garantia },
                dataAbertura = new Util().DateTimeToUnix(DateTime.Now),
                usuario = usuarioAtivo,
                ativo = true
            };
        }

        [TestMethod]
        public void Get_TiposUsuario()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.TiposUsuario();
            var tipos = resp.Content.ReadAsAsync<List<TipoUsuario>>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.IsInstanceOfType(tipos[0], typeof(TipoUsuario));
        }

        [TestMethod]
        public void Get_TipoUsuario_Administrador()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.TipoUsuario((int)TipoUsuario.enumTipoUsuario.Administrador);
            var tipos = resp.Content.ReadAsAsync<TipoUsuario>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.AreEqual(tipos.tipo.ToLower(), "administrador");
            Assert.IsInstanceOfType(tipos, typeof(TipoUsuario));
        }

        [TestMethod]
        public void Post_Cadastro_Usuario()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.Cadastrar(usuarioAtivo);
            var user = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.Created, resp.StatusCode);
        }

        [TestMethod]
        public void Post_Cadastro_Usuario_Empresa_Fake()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            usuarioAtivo.empresa.id = 9999;
            var resp = ctrl.Cadastrar(usuarioAtivo);
            var user = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.NoContent, resp.StatusCode);
            Assert.AreEqual(user, 0);
        }

        [TestMethod]
        public void Get_Usuario()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.Usuario(usuarioAtivo.id);
            var usuario = resp.Content.ReadAsAsync<Usuario>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.IsInstanceOfType(usuario, typeof(Usuario));
        }

        [TestMethod]
        public void Put_Desativar_Usuario()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.Desativar(2);

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
        }

        [TestMethod]
        public void Put_Editar_Usuario()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.Editar(usuarioInativo);
            var usuario = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.AreEqual(usuario, 1);
        }

        [TestMethod]
        public void Get_Usuarios_de_Empresa()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var resp = ctrl.Usuarios(1);
            var usuarios = resp.Content.ReadAsAsync<List<Usuario>>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.IsInstanceOfType(usuarios, typeof(List<Usuario>));
        }

        [TestMethod]
        public void Login_Usuario_Ativo()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var acesso = new Acesso {
                login = "123",
                senha = "57dca0226017e834dc0f6cc8363961ccf731bb85"
            };

            var resp = ctrl.Login(acesso);
            var usuario = resp.Content.ReadAsAsync<Usuario>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.IsInstanceOfType(usuario, typeof(Usuario));
        }

        [TestMethod]
        public void Login_Senha_Errada()
        {
            var ctrl = new UsuarioController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var acesso = new Acesso
            {
                login = "123",
                senha = "6MuCDG+hFoMPVCpAjw9B9pp6c4SffERCT9mn3CdLee8="
            };

            var resp = ctrl.Login(acesso);
            var usuario = resp.Content.ReadAsAsync<Usuario>().Result;

            Assert.AreEqual(HttpStatusCode.NoContent, resp.StatusCode);
            Assert.AreEqual(usuario.id , 0);
        }
    }
}
