﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scos.Entities;
using ScosApi.Controllers;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ScosApi.Tests
{
    [TestClass]
    public class OrdemServicoTest
    {
        [TestMethod]
        public void Post_Cadastrar_OS_Orcamento_NaoPreventiva_ComUmItem()
        {
            var ctrl = new OrdemServicoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var os = new OrdemServico
            {
                usuario = new Usuario { id = 1 },
                tipoId = (int)TipoOrdemServico.TipoOS.Orcamento,
                preventivo = false,
                empresa = new Empresa { id = 1 },
                defeitoInformado = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec."
            };

            var listItem = new List<Item>();
            listItem.Add(new Item { id = 1 });
            os.item = listItem;

            var resp = ctrl.Cadastrar(os);
            var retorno = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.Created, resp.StatusCode);
            Assert.IsTrue(retorno > 0);
        }

        [TestMethod]
        public void Post_InicioAnalise_OS()
        {
            var ctrl = new OrdemServicoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var os = new OrdemServico
            {
                id = 5,
                tecnicoInicioAnalise = 1                
            };
            

            var resp = ctrl.InicioAnalise(os);
            var retorno = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.IsTrue(retorno > 0);
        }

        [TestMethod]
        public void Post_Autorizar_OS()
        {
            var ctrl = new OrdemServicoController();
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();

            var os = new OrdemServico
            {
                id = 5,
                autorizacao = true,
                responsavelAutorizacao = "Luke Skywalker"
            };

            var resp = ctrl.Autorizacao(os);
            var retorno = resp.Content.ReadAsAsync<int>().Result;

            Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
            Assert.IsTrue(retorno > 0);
        }

    }
}
