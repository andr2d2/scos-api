﻿using System;
using Scos.Entities;
using Scos.DAO.Repository.Base;
using Dao.Context;
using System.Collections.Generic;
using System.Linq;
using Dao;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Scos.DAL.Repository
{
    public class OrdemServicoRepository : Repository<tblOrdemServico>, IOrdemServicoRepository
    {
        // construtor para setar o contexto na classe base
        public OrdemServicoRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public OrdemServico Cadastrar(OrdemServico os)
        {
            try
            {
                // info basica
                var osCadastrada = Add(new tblOrdemServico
                {
                    DataAberturaOs = DateTime.Now,
                    TipoId = os.tipo.id,
                    Preventivo = os.preventivo,
                    Emergencia = os.emergencia,
                    EmpresaId = os.empresa.id,
                    DefeitoInformado = os.defeitoInformado.Trim(),
                    DefeitoConstatado = os.defeitoConstatado.Trim()
                });

                if (os.equipamentos != null)
                {
                    //itens da os
                    foreach (var item in os.equipamentos)
                    {
                        var itemCadastrado = new Repository<tblItem>(ctx).Add(new tblItem
                        {
                            Equipamento = item.equipamento.Trim(),
                            Modelo = item.modelo.Trim(),
                            NumeroSerie = item.numeroSerie.Trim(),
                            Localizacao = item.localizacao.Trim()
                        });

                        ctx.SaveChanges();

                        new Repository<tblItemOrdemServico>(ctx).Add(new tblItemOrdemServico()
                        {
                            DataInclusao = DateTime.Now,
                            ItemId = itemCadastrado.Id,
                            OrdemServicoId = osCadastrada.Id
                        });
                    }
                }

                //usuario
                new Repository<tblUsuarioOrdemServico>(ctx).Add(
                    new tblUsuarioOrdemServico
                    {
                        DataAlteracao = DateTime.Now,
                        OrdemServicoId = osCadastrada.Id,
                        UsuarioId = os.usuario.id
                    }
                );

                //status
                new Repository<tblOrdemServicoStatus>(ctx).Add(
                    new tblOrdemServicoStatus
                    {
                        DataAlteracao = DateTime.Now,
                        OrdemServicoId = osCadastrada.Id,
                        StatusId = (int)Status.enumStatus.Aberto
                    }
                );

                ctx.SaveChanges();

                os = GetOs(osCadastrada.Id);
                new Util().EnviarEmailNovaOrdemServico(os);

                return os;
            }
            //catch (Exception ex)
            //{
            //    throw;
            //}
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Debug.WriteLine("Erro ==================================================");
                    Debug.WriteLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Debug.WriteLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                    Debug.WriteLine("======================================================");
                }

                throw;
            }
        }

        public int InicioAnalise(OrdemServico os)
        {
            try
            {
                var encerrada = (from o in ctx.tblOrdemServicoStatus
                                 where o.OrdemServicoId == os.id
                                 orderby o.DataAlteracao descending
                                 select o).FirstOrDefault().StatusId;

                if (encerrada == (int)Status.enumStatus.Encerrado)
                {
                    return 0;
                }

                var temp = Get(os.id);
                temp.DataInicioAnalise = DateTime.Now;
                temp.TecnicoInicioAnalise = os.inicioAnaliseOs.tecnicoInicioAnalise;

                Update(temp);

                //status
                new Repository<tblOrdemServicoStatus>(ctx).Add(
                    new tblOrdemServicoStatus
                    {
                        DataAlteracao = DateTime.Now,
                        OrdemServicoId = os.id,
                        StatusId = (int)Status.enumStatus.Atribuido
                    }
                );

                return ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int FimAnalise(OrdemServico os)
        {
            try
            {
                var encerrada = (from o in ctx.tblOrdemServicoStatus
                                 where o.OrdemServicoId == os.id
                                 orderby o.DataAlteracao descending
                                 select o).FirstOrDefault().StatusId;

                if (encerrada == (int)Status.enumStatus.Encerrado)
                {
                    return 0;
                }

                var temp = Get(os.id);
                temp.DataFimAnalise = DateTime.Now;
                temp.TecnicoFimAnalise = os.fimAnaliseOs.tecnicoFimAnalise;
                temp.DescricaoAnalise = os.fimAnaliseOs.descricaoAnalise.Trim();

                foreach (var p in os.fimAnaliseOs.pecas)
                {
                    var peca = new PecaRepository(ctx).Adicionar(p);

                    new Repository<tblPecaOrdemServico>(ctx).Add(new tblPecaOrdemServico
                    {
                        OrdemServicoId = os.id,
                        PecaId = peca.id,
                        Quantidade = p.quantidade
                    });
                }

                //status
                new Repository<tblOrdemServicoStatus>(ctx).Add(
                    new tblOrdemServicoStatus
                    {
                        DataAlteracao = DateTime.Now,
                        OrdemServicoId = os.id,
                        StatusId = (int)Status.enumStatus.Pendente_autorizacao
                    }
                );

                return ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int Autorizacao(OrdemServico os)
        {
            try
            {
                var encerrada = (from o in ctx.tblOrdemServicoStatus
                                 where o.OrdemServicoId == os.id
                                 orderby o.DataAlteracao descending
                                 select o).FirstOrDefault().StatusId;

                if (encerrada == (int)Status.enumStatus.Encerrado)
                {
                    return 0;
                }

                var temp = Get(os.id);
                temp.DataAutorizacao = DateTime.Now;
                temp.Autorizado = os.autorizacao.autorizacao;
                temp.ResponsavelAutorizacao = os.autorizacao.responsavelAutorizacao.Trim();

                var autorizacao = (os.autorizacao.autorizacao.Value ? (int)Status.enumStatus.Autorizado : (int)Status.enumStatus.Nao_autorizado);

                //status
                new Repository<tblOrdemServicoStatus>(ctx).Add(
                    new tblOrdemServicoStatus
                    {
                        DataAlteracao = DateTime.Now,
                        OrdemServicoId = os.id,
                        StatusId = autorizacao
                    }
                );

                ctx.SaveChanges();

                var osEmail = GetOs(os.id);
                new Util().EnviarEmailAutorizacao(osEmail);

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int Fechar(int ordemServicoId)
        {
            try
            {
                var encerrada = (from o in ctx.tblOrdemServicoStatus
                                 where o.OrdemServicoId == ordemServicoId
                                 orderby o.DataAlteracao descending
                                 select o).FirstOrDefault().StatusId;

                if (encerrada == (int)Status.enumStatus.Encerrado)
                {
                    return 0;
                }

                var tempOs = Get(ordemServicoId);
                tempOs.DataFechamentoOs = DateTime.Now;

                //status
                new Repository<tblOrdemServicoStatus>(ctx).Add(
                    new tblOrdemServicoStatus
                    {
                        DataAlteracao = DateTime.Now,
                        OrdemServicoId = ordemServicoId,
                        StatusId = (int)Status.enumStatus.Encerrado
                    }
                );

                ctx.SaveChanges();

                var osEmail = GetOs(ordemServicoId);
                new Util().EnviarEmailEncerramentoOs(osEmail);

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public List<OrdemServico> Listar()
        {
            try
            {
                var lOs = GetAll();
                List<OrdemServico> listOs = new List<OrdemServico>();
                //List<Item> lItem = new List<Item>();
                //List<Peca> lPecas = new List<Peca>();

                foreach (var os in lOs)
                {
                    var cliente = new Repository<tblEmpresa>(ctx).Get(os.EmpresaId);
                    var status = (from o in ctx.tblOrdemServicoStatus
                                  join s in ctx.tblStatus on o.StatusId equals s.Id
                                  where o.OrdemServicoId == os.Id
                                  orderby o.DataAlteracao descending
                                  select s).FirstOrDefault();

                    //var tempItens = (from it in ctx.tblItemOrdemServico where it.OrdemServicoId == os.Id select it).ToList();
                    //var tempPecas = (from p in ctx.tblPecaOrdemServico where p.OrdemServicoId == os.Id select p).ToList();
                    var user = new Repository<tblUsuarioOrdemServico>(ctx).Find(o => o.OrdemServicoId == os.Id).UsuarioId;

                    var tempAutorizacao = new Autorizacao
                    {
                        //autorizacao = os.Autorizado.HasValue,
                        //dataAutorizacao = os.DataAutorizacao.HasValue ? os.DataAutorizacao.Value : DateTime.MinValue,
                        responsavelAutorizacao = os.ResponsavelAutorizacao == null ? string.Empty : os.ResponsavelAutorizacao.Trim()
                    };

                    var tempConserto = new Conserto
                    {
                        //dataConserto = os.DataConserto.HasValue ? os.DataConserto : DateTime.MinValue,
                        //descricaoConserto = os.DescricaoConserto == null ? string.Empty : os.DescricaoConserto.Trim(),
                        tecnicoConserto = os.TecnicoConserto.HasValue ? os.TecnicoConserto.Value : 0
                    };

                    //var tempEmpresa = new Empresa
                    //{
                    //    id = empresa.Id,
                    //    cnpj = empresa.Cnpj.Trim(),
                    //    municipio = empresa.Municipio.Trim(),
                    //    nomeFantasia = empresa.NomeFantasia.Trim()
                    //};

                    var tempDeslocamento = new Deslocamaneto
                    {
                        //deslocamentoChegada = os.DeslocamentoChegada.HasValue ? os.DeslocamentoChegada.Value : DateTime.MinValue,
                        //deslocamentoSaida = os.DeslocamentoSaida.HasValue ? os.DeslocamentoSaida.Value : DateTime.MinValue,
                        deslocamentoTotal = os.DeslocamentoTotal.HasValue ? os.DeslocamentoTotal.Value : DateTime.MinValue,
                        //kmFinal = os.KmFinal.HasValue ? os.KmFinal.Value : 0,
                        //kmInicial = os.KmInicial.HasValue ? os.KmInicial.Value : 0,
                        kmTotal = os.KmTotal.HasValue ? os.KmTotal.Value : 0
                    };

                    var tempFimAnalise = new FimAnaliseOs
                    {
                        //dataFimAnalise = os.DataFimAnalise.HasValue ? os.DataFimAnalise.Value : DateTime.MinValue,
                        //descricaoAnalise = os.DescricaoAnalise == null ? String.Empty : os.DescricaoAnalise.Trim(),
                        tecnicoFimAnalise = os.TecnicoFimAnalise.HasValue ? os.TecnicoFimAnalise.Value : 0,
                        //pecas = lPecas
                    };

                    var tempInicioAnalise = new InicioAnaliseOs
                    {
                        //dataInicioAnalise = os.DataInicioAnalise.HasValue ? os.DataInicioAnalise.Value : DateTime.MinValue,
                        tecnicoInicioAnalise = os.TecnicoInicioAnalise.HasValue ? os.TecnicoInicioAnalise.Value : 0
                    };

                    //if (tempItens != null)
                    //{
                    //    foreach (var item in tempItens)
                    //    {
                    //        lItem.Add(new Item { id = item.id });
                    //    }
                    //}


                    //if (tempPecas != null)
                    //{
                    //    foreach (var p in tempPecas)
                    //    {
                    //        lPecas.Add(new Peca { id = p.Id });
                    //    }
                    //}


                    listOs.Add(new OrdemServico
                    {
                        id = os.Id,
                        preventivo = os.Preventivo,
                        emergencia = os.Emergencia,
                        dataAberturaOs = os.DataAberturaOs,
                        dataFechamentoOs = os.DataFechamentoOs,
                        tempoAberto = String.Empty,
                        defeitoInformado = os.DefeitoInformado.Trim(),
                        defeitoConstatado = os.DefeitoConstatado.Trim(),
                        //item = lItem,
                        autorizacao = tempAutorizacao,
                        conserto = tempConserto,
                        deslocamaneto = tempDeslocamento,
                        //empresa = tempEmpresa,
                        status = new Status
                        {
                            id = status.Id,
                            status = status.Status.Trim()
                        },
                        tipo = new TipoOrdemServico
                        {
                            tipo = new Repository<tblTipoOrdemServico>(ctx).Get(os.TipoId).Tipo
                        },
                        fimAnaliseOs = tempFimAnalise,
                        inicioAnaliseOs = tempInicioAnalise,
                        usuario = new Usuario
                        {
                            id = user
                        }

                    });
                }

                return listOs.OrderByDescending(d => d.dataAberturaOs).ToList();

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public OrdemServico GetOs(int osId)
        {
            try
            {
                var tempOs = Get(osId);


                List<Item> lItem = new List<Item>();
                List<Peca> lPecas = new List<Peca>();


                var empresa = new Repository<tblEmpresa>(ctx).Get(tempOs.EmpresaId);
                var status = (from o in ctx.tblOrdemServicoStatus
                              join s in ctx.tblStatus on o.StatusId equals s.Id
                              where o.OrdemServicoId == osId
                              orderby o.DataAlteracao descending
                              select s).FirstOrDefault();

                var tempItens = (from it in ctx.tblItemOrdemServico where it.OrdemServicoId == osId select it).ToList();
                var tempPecas = (from p in ctx.tblPecaOrdemServico where p.OrdemServicoId == osId select p).ToList();

                var user = (from uo in ctx.tblUsuarioOrdemServico
                            join u in ctx.tblUsuario on uo.UsuarioId equals u.Id
                            where uo.OrdemServicoId == osId
                            select u).FirstOrDefault();



                var tempAutorizacao = new Autorizacao
                {
                    autorizacao = tempOs.Autorizado.HasValue && tempOs.Autorizado.Value ? true : false,
                    dataAutorizacao = tempOs.DataAutorizacao.HasValue ? tempOs.DataAutorizacao.Value : DateTime.MinValue,
                    responsavelAutorizacao = tempOs.ResponsavelAutorizacao == null ? string.Empty : tempOs.ResponsavelAutorizacao.Trim()
                };

                var tempConserto = new Conserto
                {
                    nomeTecnicoConserto = tempOs.TecnicoConserto.HasValue ? new Repository<tblUsuario>(ctx).Get(tempOs.TecnicoConserto.Value).NomeCompleto : string.Empty,
                    dataConserto = tempOs.DataConserto.HasValue ? tempOs.DataConserto : DateTime.MinValue,
                    descricaoConserto = tempOs.DescricaoConserto == null ? string.Empty : tempOs.DescricaoConserto.Trim(),
                    tecnicoConserto = tempOs.TecnicoConserto.HasValue ? tempOs.TecnicoConserto.Value : 0
                };

                var tempEmpresa = new Empresa
                {
                    id = empresa.Id,
                    cnpj = empresa.Cnpj.Trim(),
                    nomeFantasia = empresa.NomeFantasia.Trim(),
                    endereco = empresa.Endereco.Trim(),
                    bairro = empresa.Bairro.Trim(),
                    cep = empresa.Cep.Trim(),
                    cidade = new Cidade { nomeCidade = new Repository<tblCidade>(ctx).Get(empresa.CidadeId).Cidade },
                    estado = new Estado { nomeEstado = new Repository<tblEstado>(ctx).Get(empresa.EstadoId).Estado },
                    numero = empresa.Numero.Trim(),
                    complemento = empresa.Complemento.Trim(),
                    razaoSocial = empresa.RazaoSocial.Trim(),
                    telContato = empresa.TelContato.Trim(),
                    ativo = empresa.Ativo
                };

                var tempDeslocamento = new Deslocamaneto
                {
                    deslocamentoChegada = tempOs.DeslocamentoChegada.HasValue ? tempOs.DeslocamentoChegada.Value : DateTime.MinValue,
                    deslocamentoSaida = tempOs.DeslocamentoSaida.HasValue ? tempOs.DeslocamentoSaida.Value : DateTime.MinValue,
                    deslocamentoTotal = tempOs.DeslocamentoTotal.HasValue ? tempOs.DeslocamentoTotal.Value : DateTime.MinValue,
                    kmFinal = tempOs.KmFinal.HasValue ? tempOs.KmFinal.Value : 0,
                    kmInicial = tempOs.KmInicial.HasValue ? tempOs.KmInicial.Value : 0,
                    kmTotal = tempOs.KmTotal.HasValue ? tempOs.KmTotal.Value : 0
                };

                var tempFimAnalise = new FimAnaliseOs
                {
                    nomeTecnicoFimAnalise = tempOs.TecnicoFimAnalise.HasValue ? new Repository<tblUsuario>(ctx).Get(tempOs.TecnicoFimAnalise.Value).NomeCompleto : string.Empty,
                    dataFimAnalise = tempOs.DataFimAnalise.HasValue ? tempOs.DataFimAnalise.Value : DateTime.MinValue,
                    descricaoAnalise = tempOs.DescricaoAnalise == null ? String.Empty : tempOs.DescricaoAnalise.Trim(),
                    tecnicoFimAnalise = tempOs.TecnicoFimAnalise.HasValue ? tempOs.TecnicoFimAnalise.Value : 0,
                    pecas = lPecas
                };

                var tempInicioAnalise = new InicioAnaliseOs
                {
                    nomeTecnicoInicioAnalise = tempOs.TecnicoInicioAnalise.HasValue ? new Repository<tblUsuario>(ctx).Get(tempOs.TecnicoInicioAnalise.Value).NomeCompleto : string.Empty,
                    dataInicioAnalise = tempOs.DataInicioAnalise.HasValue ? tempOs.DataInicioAnalise.Value : DateTime.MinValue,
                    tecnicoInicioAnalise = tempOs.TecnicoInicioAnalise.HasValue ? tempOs.TecnicoInicioAnalise.Value : 0
                };

                if (tempItens.Count > 0)
                {
                    foreach (var item in tempItens)
                    {
                        var tempItem = new Item();
                        tempItem = new ItemRepository(ctx).GetItem(item.ItemId);

                        lItem.Add(new Item
                        {
                            id = tempItem.id,
                            equipamento = tempItem.equipamento,
                            modelo = tempItem.modelo,
                            numeroSerie = tempItem.numeroSerie,
                            localizacao = tempItem.localizacao
                        });
                    }
                }


                if (tempPecas.Count > 0)
                {
                    foreach (var p in tempPecas)
                    {
                        var tempPeca = new Repository<tblPeca>(ctx).Get(p.PecaId);

                        lPecas.Add(new Peca
                        {
                            id = tempPeca.Id,
                            peca = tempPeca.Peca,
                            codigo = tempPeca.Codigo,
                            quantidade = p.Quantidade
                        });
                    }
                }


                var os = new OrdemServico()
                {
                    id = tempOs.Id,
                    preventivo = tempOs.Preventivo,
                    emergencia = tempOs.Emergencia,
                    dataAberturaOs = tempOs.DataAberturaOs,                    
                    dataFechamentoOs = tempOs.DataFechamentoOs,
                    defeitoConstatado = tempOs.DefeitoConstatado,
                    defeitoInformado = tempOs.DefeitoInformado.Trim(),
                    equipamentos = lItem,
                    autorizacao = tempAutorizacao,
                    conserto = tempConserto,
                    deslocamaneto = tempDeslocamento,
                    empresa = tempEmpresa,
                    status = new Status
                    {
                        id = status.Id,
                        status = status.Status.Trim()
                    },
                    tipo = new TipoOrdemServico
                    {
                        id = tempOs.TipoId,
                        tipo = new Repository<tblTipoOrdemServico>(ctx).Get(tempOs.TipoId).Tipo
                    },
                    fimAnaliseOs = tempFimAnalise,
                    inicioAnaliseOs = tempInicioAnalise,
                    usuario = new Usuario
                    {
                        id = user.Id,
                        nomeCompleto = user.NomeCompleto
                    }
                };

                if (os.status.id == (int)Status.enumStatus.Encerrado)
                    os.tempoAberto = new Util().TimestampToStringFormatada(os.dataFechamentoOs.Value, os.dataAberturaOs);
                else
                    os.tempoAberto = new Util().TimestampToStringFormatada(DateTime.Now, os.dataAberturaOs);

                return os;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public int Conserto(OrdemServico os)
        {
            try
            {
                var encerrada = (from o in ctx.tblOrdemServicoStatus
                                 where o.OrdemServicoId == os.id
                                 orderby o.DataAlteracao descending
                                 select o).FirstOrDefault().StatusId;

                if (encerrada == (int)Status.enumStatus.Encerrado)
                {
                    return 0;
                }

                var temp = Get(os.id);
                temp.DataConserto = DateTime.Now;
                temp.TecnicoConserto = os.conserto.tecnicoConserto;
                temp.DescricaoConserto = os.conserto.descricaoConserto.Trim();

                //status
                new Repository<tblOrdemServicoStatus>(ctx).Add(
                    new tblOrdemServicoStatus
                    {
                        DataAlteracao = DateTime.Now,
                        OrdemServicoId = os.id,
                        StatusId = (int)Status.enumStatus.Recuperado
                    }
                );

                Update(temp);

                return ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int Deslocamento(OrdemServico os)
        {
            try
            {
                var encerrada = (from o in ctx.tblOrdemServicoStatus
                                 where o.OrdemServicoId == os.id
                                 orderby o.DataAlteracao descending
                                 select o).FirstOrDefault().StatusId;

                if (encerrada == (int)Status.enumStatus.Encerrado)
                {
                    return 0;
                }

                var temp = Get(os.id);
                temp.DeslocamentoSaida = Convert.ToDateTime(os.deslocamaneto.deslocamentoSaida.Value.ToString("yyyy - MM - dd HH:mm:ss"));
                temp.DeslocamentoChegada = Convert.ToDateTime(os.deslocamaneto.deslocamentoChegada.Value.ToString("yyyy - MM - dd HH:mm:ss"));
                temp.DeslocamentoTotal = Convert.ToDateTime(os.deslocamaneto.deslocamentoTotal.Value.ToString("HH:mm:ss"));

                temp.KmFinal = os.deslocamaneto.kmFinal;
                temp.KmInicial = os.deslocamaneto.kmInicial;
                temp.KmTotal = os.deslocamaneto.kmTotal;

                Update(temp);

                return ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int Editar(OrdemServico os)
        {
            try
            {
                var status = (from o in ctx.tblOrdemServicoStatus
                              where o.OrdemServicoId == os.id
                              orderby o.DataAlteracao descending
                              select o).FirstOrDefault().StatusId;

                if (status != (int)Status.enumStatus.Aberto && status != (int)Status.enumStatus.Atribuido)
                    return 0;

                var editOs = Find(o => o.Id == os.id);
                editOs.Id = os.id;
                editOs.TipoId = os.tipo.id;
                editOs.Preventivo = os.preventivo;
                editOs.Emergencia = os.emergencia;
                editOs.EmpresaId = os.empresa.id;
                editOs.DefeitoInformado = os.defeitoInformado;
                editOs.DefeitoConstatado = os.defeitoConstatado;

                Update(editOs);

                var idsItensOrdemServico = (from i in ctx.tblItemOrdemServico
                                            where i.OrdemServicoId == os.id
                                            select i.ItemId).ToList();

                var itensExistentes = os.equipamentos.FindAll(e => e.id != 0);
                var equipamentosNovos = os.equipamentos.FindAll(e => e.id == 0);
                var idsItensDeletados = idsItensOrdemServico.Where(e => !itensExistentes.Any(i => e == i.id));

                foreach (var id in idsItensDeletados)
                {
                    var obj = new Repository<tblItemOrdemServico>(ctx).Find(i => i.ItemId == id);
                    new Repository<tblItemOrdemServico>(ctx).Delete(obj);
                }

                foreach (var item in equipamentosNovos)
                {
                    var id = new ItemRepository(ctx).Cadastrar(item);
                    new Repository<tblItemOrdemServico>(ctx).Add(new tblItemOrdemServico
                    {
                        DataInclusao = DateTime.Now,
                        ItemId = id,
                        OrdemServicoId = os.id
                    });
                }

                ctx.SaveChanges();

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }
    }
}
