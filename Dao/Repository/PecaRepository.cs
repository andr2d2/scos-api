﻿using Dao.Context;
using Scos.DAO.Repository.Base;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scos.DAL.Repository
{
    public class PecaRepository : Repository<tblPeca>, IPecaRepository
    {
        // construtor para setar o contexto na classe base
        public PecaRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public Peca Adicionar(Peca peca)
        {
            try
            {
                var pecaRetorno = new Peca();
                var tempPeca = Find(p => p.Codigo == peca.codigo.Trim());

                if (tempPeca == null)
                {
                    tempPeca = Add(new tblPeca
                    {
                        Codigo = peca.codigo.Trim(),
                        Peca = peca.peca.Trim()
                    });

                    ctx.SaveChanges();
                }

                pecaRetorno.id = tempPeca.Id;
                pecaRetorno.peca = tempPeca.Peca.Trim();
                pecaRetorno.codigo = tempPeca.Codigo.Trim();

                return pecaRetorno;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Peca GetPeca(int pecaId)
        {
            try
            {
                var tempPeca = Get(pecaId);

                var retornoPeca = new Peca {
                    codigo = tempPeca.Codigo,
                    id = tempPeca.Id,
                    peca = tempPeca.Peca
                };

                return retornoPeca;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
