﻿using Dao.Context;
using Scos.DAO.Repository.Base;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scos.DAL.Repository
{
    public class UtilRepository : Repository<tblItem>, IUtilRepository
    {
        // construtor para setar o contexto na classe base
        public UtilRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public List<Status> ListarStatus()
        {
            try
            {
                var lStatus = new Repository<tblStatus>(ctx).GetAll();
                List<Status> lista = new List<Status>();

                foreach (var st in lStatus)
                {
                    lista.Add(new Status {
                        id = st.Id,
                        status = st.Status.Trim()
                    });
                }

                return lista;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<TipoOrdemServico> ListarTipoOs()
        {
            try
            {
                var ltipoOs = new Repository<tblTipoOrdemServico>(ctx).GetAll();
                List<TipoOrdemServico> lTipo = new List<TipoOrdemServico>();

                foreach (var tp in ltipoOs)
                {
                    lTipo.Add(new TipoOrdemServico {
                        id = tp.Id,
                        tipo = tp.Tipo.Trim()
                    });
                }

                return lTipo;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
