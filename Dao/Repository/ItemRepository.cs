﻿using Dao.Context;
using Scos.DAO.Repository.Base;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scos.DAL.Repository
{
    public class ItemRepository : Repository<tblItem>, IItemRepository
    {
        // construtor para setar o contexto na classe base
        public ItemRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public Item Adicionar(Item item)
        {
            try
            {
                var itemAdicionado = Add(new tblItem
                {
                    NumeroSerie = item.numeroSerie.Trim(),
                    Equipamento = item.equipamento.Trim(),
                    Modelo = item.modelo.Trim()
                });

                item.id = itemAdicionado.Id;

                // adicionar item ao chamado
                new Repository<tblItemChamado>(ctx).Add(new tblItemChamado()
                {
                    //ChamadoId = item.chamadoId,
                    ItemId = item.id
                });

                ctx.SaveChanges();

                return item;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public int Cadastrar(Item item)
        {
            try
            {
                var obj = new tblItem
                {
                    Equipamento = item.equipamento.Trim(),
                    Modelo = item.modelo.Trim(),
                    NumeroSerie = item.numeroSerie.Trim(),
                    Localizacao = item.localizacao.Trim()
                };

                var itemCadastrado = Add(obj);

                ctx.SaveChanges();

                return itemCadastrado.Id;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public void Excluir(Item item)
        {
            try
            {
                new Repository<tblItemChamado>(ctx).Delete(new tblItemChamado
                {
                    ItemId = item.id
                });

                ctx.SaveChanges();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<Item> Itens()
        {
            try
            {
                var listItens = new List<Item>();
                var itens = GetAll().ToList();

                foreach (var item in itens)
                {
                    listItens.Add(new Item
                    {
                        equipamento = item.Equipamento.Trim(),
                        id = item.Id,
                        numeroSerie = item.NumeroSerie.Trim(),
                        modelo = item.Modelo.Trim()
                    });
                }

                return listItens;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Item GetItem(int itemId)
        {
            try
            {
                var listItens = new List<Item>();
                var item = Get(itemId);


                var retornoItem = new Item
                {
                    equipamento = item.Equipamento.Trim(),
                    id = item.Id,
                    numeroSerie = item.NumeroSerie.Trim(),
                    modelo = item.Modelo.Trim(), 
                    localizacao = item.Localizacao.Trim()
                };

                return retornoItem;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<Item> GetItensChamado(int chamadoId)
        {
            try
            {
                var listItens = (from c in ctx.tblItemChamado
                                 join i in ctx.tblItem on c.ItemId equals i.Id
                                 where c.ChamadoId == chamadoId
                                 select new Item {
                                     equipamento = i.Equipamento.Trim(),
                                     modelo = i.Modelo.Trim(),
                                     numeroSerie = i.NumeroSerie.Trim(),
                                     id = i.Id
                                 }).ToList();

                

                return listItens;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
