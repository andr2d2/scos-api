﻿using Dao.Context;
using Scos.DAO.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;

namespace Scos.DAL.Repository
{
    public interface IEmpresaRepository : IRepository<tblEmpresa>
    {
        int Cadastrar(Empresa cliente);
        List<Empresa> Clientes(int empresaId, bool ativo = true);
        List<Empresa> Empresas();
        Empresa Cliente(int empresaId);
        int Editar(Empresa empresa);
        int Desativar(int id);
    }
}
