﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Scos.DAO.Repository.Base
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        TEntity Get(int id);
        TEntity Find(Expression <Func<TEntity, bool>> predicate);
        TEntity Add(TEntity obj);
        void Delete(TEntity obj);
        void Update(TEntity obj);
    }
}
