﻿using System;
using Scos.Entities;
using System.Linq;
using System.Collections.Generic;
using Scos.DAO.Repository.Base;
using Dao.Context;
using Model;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Scos.DAL.Repository
{
    public class ChamadoRepository : Repository<tblChamado>, IChamadoRepository
    {
        // construtor para setar o contexto na classe base
        public ChamadoRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public Chamado Cadastrar(Chamado chamado)
        {
            try
            {
                // verifica se usuário é existente e ativo
                var usuarioExistente = new Repository<tblUsuario>(ctx).Get(chamado.usuario.id);

                if (usuarioExistente == null || !usuarioExistente.Ativo)
                    return new Chamado();


                // adiciona chamado   
                var chamadoAdicionado = Add(new tblChamado()
                {
                    DataAbertura = DateTime.Now,
                    Descricao = chamado.descricao.Trim(),
                    Ativo = true
                });

                // itens do chamado se houver
                if (chamado.itens != null)
                {
                    foreach (var item in chamado.itens)
                    {
                        var itemAdicionado = new Repository<tblItem>(ctx).Add(new tblItem
                        {
                            Equipamento = item.equipamento.Trim(),
                            Modelo = item.modelo.Trim(),
                            NumeroSerie = item.numeroSerie.Trim()
                        });

                        ctx.SaveChanges();

                        new Repository<tblItemChamado>(ctx).Add(new tblItemChamado
                        {
                            ChamadoId = chamadoAdicionado.Id,
                            ItemId = itemAdicionado.Id
                        });
                    }
                }

                // chamado ao usuario
                new Repository<tblChamadoUsuario>(ctx).Add(new tblChamadoUsuario
                {
                    ChamadoId = chamadoAdicionado.Id,
                    UsuarioId = chamado.usuario.id
                });

                // status
                new Repository<tblChamadoStatus>(ctx).Add(new tblChamadoStatus
                {
                    ChamadoId = chamadoAdicionado.Id,
                    StatusId = (int)Status.enumStatus.Aberto,
                    DataAlteracao = DateTime.Now
                });

                ctx.SaveChanges();

                return Chamado(chamadoAdicionado.Id);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Chamado Chamado(int id)
        {
            try
            {
                var chamado = Chamados().Find(c => c.id == id);
                return chamado;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<ChamadoMap> PinChamados()
        {
            try
            {
                var pins = (from c in ctx.tblChamado
                            join cu in ctx.tblChamadoUsuario on c.Id equals cu.ChamadoId
                            join u in ctx.tblUsuario on cu.UsuarioId equals u.Id
                            join e in ctx.tblEmpresa on u.EmpresaId equals e.Id
                            join s in ctx.tblChamadoStatus on c.Id equals s.ChamadoId
                            select new ChamadoMap
                            {
                                chamadoId = c.Id,
                                empresa = new Empresa
                                {
                                    nomeFantasia = e.NomeFantasia.Trim(),
                                    endereco = e.Endereco.Trim(),
                                    numero = e.Numero.Trim(),
                                    complemento = e.Complemento.Trim(),
                                    bairro = e.Bairro.Trim(),
                                    coordenadas = new Coordenadas
                                    {
                                        latitude = e.latitude,
                                        longitude = e.longitude
                                    }
                                }
                            }).ToList();

                foreach (var pin in pins)
                {
                    pin.status = (from cs in ctx.tblChamadoStatus
                                  join s in ctx.tblStatus on cs.StatusId equals s.Id
                                  where cs.ChamadoId == pin.chamadoId
                                  orderby cs.DataAlteracao descending
                                  select new Status
                                  {
                                      id = s.Id,
                                      status = s.Status.Trim()
                                  }).FirstOrDefault();
                }

                return pins;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ChamadoMap PinChamado(int chamadoId)
        {
            try
            {
                var pin = PinChamados().Find(c => c.chamadoId == chamadoId);
                return pin;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public List<Chamado> Chamados()
        {
            try
            {
                var listChamados = (from cham in ctx.tblChamado
                                    join chamUser in ctx.tblChamadoUsuario on cham.Id equals chamUser.ChamadoId
                                    join user in ctx.tblUsuario on chamUser.UsuarioId equals user.Id
                                    join emp in ctx.tblEmpresa on user.EmpresaId equals emp.Id
                                    where cham.Ativo == true
                                    select new Chamado
                                    {
                                        ativo = cham.Ativo,
                                        dataAbertura = cham.DataAbertura,
                                        dataFechamento = cham.DataFechamento.HasValue ? cham.DataFechamento : null,
                                        descricao = cham.Descricao.Trim(),
                                        id = cham.Id,
                                        usuario = new Usuario
                                        {
                                            id = chamUser.UsuarioId,
                                            nomeCompleto = user.NomeCompleto,
                                            email = user.Email.Trim(),
                                            empresa = new Empresa
                                            {
                                                id = emp.Id,
                                                nomeFantasia = emp.NomeFantasia.Trim(),
                                                cnpj = emp.Cnpj.Trim(),
                                            }
                                        }
                                    }).ToList();

                foreach (var chamado in listChamados)
                {
                    chamado.status = (from cs in ctx.tblChamadoStatus
                                      join s in ctx.tblStatus on cs.StatusId equals s.Id
                                      where cs.ChamadoId == chamado.id
                                      orderby cs.DataAlteracao descending
                                      select new Status
                                      {
                                          id = s.Id,
                                          status = s.Status.Trim()
                                      }).FirstOrDefault();

                    chamado.itens = new ItemRepository(ctx).GetItensChamado(chamado.id);

                    //chamado.ordensServico = (from osCharm in ctx.tblChamadoOrdemServico
                    //                         join os in ctx.tblOrdemServico on osCharm.OrdemServicoId equals os.Id
                    //                         join u in ctx.tblUsuario on os.TecnicoConserto equals u.Id
                    //                         join uf in ctx.tblUsuario on os.TecnicoFimAnalise equals uf.Id
                    //                         join ui in ctx.tblUsuario on os.TecnicoInicioAnalise equals ui.Id
                    //                         where osCharm.ChamadoId == chamado.id
                    //                         select new OrdemServico
                    //                         {
                    //                             id = os.Id,
                    //                             dataAberturaOs = os.DataAberturaOs,
                    //                             dataFechamentoOs = os.DataFechamentoOs.HasValue? os.DataFechamentoOs : null,
                    //                             defeitoInformado = os.DefeitoInformado.Trim(),
                    //                             preventivo = os.Preventivo,
                    //                             tipo = 
                    //                             autorizacao = new Autorizacao
                    //                             {
                    //                                 autorizacao = os.Autorizado.HasValue? os.Autorizado : null,
                    //                                 dataAutorizacao = os.DataAutorizacao.HasValue? os.DataAutorizacao : null,
                    //                                 responsavelAutorizacao = string.IsNullOrEmpty(os.ResponsavelAutorizacao)? string.Empty : os.ResponsavelAutorizacao.Trim()
                    //                             },
                    //                             conserto = new Conserto
                    //                             {
                    //                                 dataConserto = os.DataConserto.HasValue? os.DataConserto : null,
                    //                                 descricaoConserto = string.IsNullOrEmpty(os.DescricaoConserto)? string.Empty : os.DescricaoConserto.Trim(),
                    //                                 nomeTecnicoConserto = u.NomeCompleto.Trim(),
                    //                                 tecnicoConserto = u.Id
                    //                             },




                    //                         }).ToList();
                }


                return listChamados;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<Chamado> ChamadosPorEmpresa(int empresaId)
        {
            try
            {
                var chamados = Chamados().FindAll(c => c.usuario.empresa.id == empresaId);
                return chamados;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<Chamado> ChamadosPorUsuario(int usuarioId)
        {
            try
            {
                var chamados = Chamados().FindAll(c => c.usuario.id == usuarioId);
                return chamados;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Chamado Editar(Chamado chamado)
        {
            try
            {
                Update(new tblChamado
                {
                    Ativo = chamado.ativo,
                    Descricao = chamado.descricao.Trim(),
                    Id = chamado.id,
                    DataAbertura = chamado.dataAbertura
                });


                var itensNovos = chamado.itens.Where(item => item.id == 0).ToList();
                var itensNaoNovos = chamado.itens.Where(item => item.id != 0).ToList();
                var chamadoItensExistente = new ItemRepository(ctx).GetItensChamado(chamado.id);
                var diferenca = chamadoItensExistente.Where(item => !itensNaoNovos.Select(i => i.id).Contains(item.id)).ToList();

                if (diferenca.Count > 0)
                {
                    foreach (var item in diferenca)
                    {
                        var itemChamado = new Repository<tblItemChamado>(ctx).Find(it => it.ItemId == item.id);
                        new Repository<tblItemChamado>(ctx).Delete(itemChamado);
                    }
                }

                if (itensNovos.Count > 0)
                {
                    foreach (var item in itensNovos)
                    {
                        var itemcadastrado = new ItemRepository(ctx).Cadastrar(item);
                        new Repository<tblItemChamado>(ctx).Add(new tblItemChamado
                        {
                            ChamadoId = chamado.id,
                            ItemId = itemcadastrado
                        });
                    }
                }

                ctx.SaveChanges();

                var chamadoEditado = Chamado(chamado.id);
                return chamadoEditado;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int Fechar(ChamadoUsuario chamadoUsuario)
        {
            try
            {
                //// verifica OS abertas e as fecha
                //var ordensServico = (from co in ctx.tblChamadoOrdemServico
                //                     join os in ctx.tblOrdemServico on co.OrdemServicoId equals os.Id
                //                     where co.ChamadoId == chamadoUsuario.chamadoId && os.StatusId != (int)Status.enumStatus.Aberto
                //                     select new OrdemServico
                //                     {
                //                         id = os.Id
                //                     }).ToList();

                //foreach (var os in ordensServico)
                //{
                //    //fechar OS's
                //}

                // fecha chamado
                var chamado = Get(chamadoUsuario.chamadoId);
                chamado.DataFechamento = DateTime.Now;
                //chamado.StatusId = (int)Status.enumStatus.Fechado;
                chamado.Ativo = false;

                Update(chamado);

                ctx.SaveChanges();

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

    }
}
