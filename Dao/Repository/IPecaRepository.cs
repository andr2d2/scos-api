﻿using Dao.Context;
using Scos.DAO.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;

namespace Scos.DAL.Repository
{
    public interface IPecaRepository : IRepository<tblPeca>
    {                       
    	Peca Adicionar(Peca peca);                 
    }
}
