﻿using Dao.Context;
using Scos.DAO.Repository.Base;
using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Scos.DAL.Repository
{
    public class UsuarioRepository : Repository<tblUsuario>, IUsuarioRepository
    {
        // construtor para setar o contexto na classe base
        public UsuarioRepository(scosEntities context) : base(context) { }

        // get o contexto setado na classe base
        public scosEntities ctx { get { return db as scosEntities; } }

        public int Cadastrar(Usuario usuario)
        {
            try
            {
                //empresa ativa
                var empresaAtiva = new Repository<tblEmpresa>(ctx).Get(usuario.empresa.id);

                if (empresaAtiva == null || !empresaAtiva.Ativo)
                    return 0;

                var novoUsuario = Add(new tblUsuario()
                {
                    Ativo = true,
                    Cpf = usuario.cpf.Trim(),
                    Email = usuario.email.Trim(),
                    EmpresaId = usuario.empresa.id,
                    NomeCompleto = usuario.nomeCompleto.Trim(),
                    TelContato = usuario.telContato.Trim(),
                    TipoId = usuario.tipoUsuario.id
                });

                var acesso = new Repository<tblAcesso>(ctx).Add(new tblAcesso
                {
                    Chave = string.Empty,
                    Login = novoUsuario.Cpf.Trim(),
                    PrimeiroAcesso = true,
                    UltimaAlteracao = DateTime.Now,
                    UsuarioId = novoUsuario.Id
                });

                ctx.SaveChanges();

                return novoUsuario.Id;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
            //catch (DbEntityValidationException e)
            //{
            //    foreach (var eve in e.EntityValidationErrors)
            //    {
            //        Debug.WriteLine("Erro ==================================================");
            //        Debug.WriteLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State));
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Debug.WriteLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage));
            //        }
            //        Debug.WriteLine("======================================================");
            //    }
            //    return 0;
            //    throw;
            //}
        }

        public int Desativar(int id)
        {
            try
            {
                var usuario = Get(id);
                usuario.Ativo = false;
                Update(usuario);

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            };
        }

        public int Editar(Usuario usuario)
        {
            try
            {
                Update(new tblUsuario
                {
                    Ativo = usuario.ativo,
                    Cpf = usuario.cpf,
                    Email = usuario.email,
                    EmpresaId = usuario.empresa.id,
                    Id = usuario.id,
                    NomeCompleto = usuario.nomeCompleto,
                    TelContato = usuario.telContato,
                    TipoId = usuario.tipoUsuario.id
                });

                ctx.SaveChanges();

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public Usuario Login(Acesso acesso)
        {
            try
            {
                byte[] salt = Encoding.UTF8.GetBytes("rayfenscos");
                var hashedPassword = HashPasswordWithSalt(Encoding.UTF8.GetBytes(acesso.senha), salt);
                var hash = Convert.ToBase64String(hashedPassword);

                var loginUser = (from a in ctx.tblAcesso
                                 join u in ctx.tblUsuario on a.UsuarioId equals u.Id
                                 where u.Ativo == true && a.Login == acesso.login && a.Chave == hash && a.PrimeiroAcesso == false
                                 select new Usuario { id = u.Id }).FirstOrDefault();

                if (loginUser != null && loginUser.id > 0)
                    return Usuario(loginUser.id);
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<TipoUsuario> TiposUsuario()
        {
            try
            {
                var listTipos = (from t in ctx.tblTipoUsuario
                                 where t.Id > 1
                                 select new TipoUsuario
                                 {
                                     id = t.Id,
                                     tipo = t.Tipo.Trim()
                                 }).ToList();

                return listTipos;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public TipoUsuario TipoUsuario(int id)
        {
            try
            {
                var tipo = new Repository<tblTipoUsuario>(ctx).Get(id);
                var tipoUsuario = new TipoUsuario
                {
                    id = tipo.Id,
                    tipo = tipo.Tipo.Trim()
                };

                return tipoUsuario;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Usuario Usuario(int id)
        {
            try
            {
                var usuario = (from u in ctx.tblUsuario
                               join t in ctx.tblTipoUsuario on u.TipoId equals t.Id
                               join e in ctx.tblEmpresa on u.EmpresaId equals e.Id
                               where u.Id == id
                               select new Usuario
                               {
                                   ativo = u.Ativo,
                                   cpf = u.Cpf,
                                   email = u.Email,
                                   empresa = new Empresa
                                   {
                                       id = u.EmpresaId,
                                       nomeFantasia = e.NomeFantasia.Trim()
                                   },
                                   id = u.Id,
                                   nomeCompleto = u.NomeCompleto,
                                   telContato = u.TelContato,
                                   tipoUsuario = new TipoUsuario
                                   {
                                       id = t.Id,
                                       tipo = t.Tipo
                                   }
                               }).FirstOrDefault();

                return usuario;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<Usuario> Usuarios(int empresaId)
        {
            try
            {
                var usuarios = (from u in ctx.tblUsuario
                                join t in ctx.tblTipoUsuario on u.TipoId equals t.Id
                                join e in ctx.tblEmpresa on u.EmpresaId equals e.Id
                                where u.EmpresaId == empresaId && e.Ativo == true && u.TipoId > 1
                                select new Usuario
                                {
                                    ativo = u.Ativo,
                                    cpf = u.Cpf,
                                    email = u.Email,
                                    empresa = new Empresa { id = u.EmpresaId },
                                    id = u.Id,
                                    nomeCompleto = u.NomeCompleto,
                                    telContato = u.TelContato,
                                    tipoUsuario = new TipoUsuario
                                    {
                                        id = t.Id,
                                        tipo = t.Tipo
                                    }
                                }).ToList();

                return usuarios;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int ResetSenha(int usuarioId)
        {
            try
            {
                var usuario = Get(usuarioId);
                var acessoTemp = new Repository<tblAcesso>(ctx).Find(l => l.UsuarioId == usuario.Id);
                acessoTemp.Chave = string.Empty;
                acessoTemp.PrimeiroAcesso = true;
                acessoTemp.UltimaAlteracao = DateTime.Now;

                new Repository<tblAcesso>(ctx).Update(acessoTemp);

                ctx.SaveChanges();

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public Usuario PrimeiroAcesso(Acesso acesso)
        {
            try
            {
                byte[] salt = Encoding.UTF8.GetBytes("rayfenscos");
                var hashedPassword = HashPasswordWithSalt(Encoding.UTF8.GetBytes(acesso.senha), salt);
                var hash = Convert.ToBase64String(hashedPassword);
                
                var acessoTemp = (from u in ctx.tblUsuario
                         join a in ctx.tblAcesso on u.Id equals a.UsuarioId
                         where u.Cpf == acesso.login
                         select a).FirstOrDefault();

                if (!acessoTemp.PrimeiroAcesso)
                {
                    return null;
                }

                acessoTemp.Chave = hash;
                acessoTemp.PrimeiroAcesso = false;
                acessoTemp.UltimaAlteracao = DateTime.Now;

                new Repository<tblAcesso>(ctx).Update(acessoTemp);

                ctx.SaveChanges();

                var usuario = new UsuarioRepository(ctx).Usuario(acessoTemp.UsuarioId);

                return usuario;

            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        private static byte[] Combine(byte[] first, byte[] second)
        {
            var ret = new byte[first.Length + second.Length];

            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);

            return ret;
        }

        private static byte[] HashPasswordWithSalt(byte[] toBeHashed, byte[] salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var combinedHash = Combine(toBeHashed, salt);

                return sha256.ComputeHash(combinedHash);
            }
        }
    }
}
