﻿using Dao.Context;
using Scos.DAO.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;

namespace Scos.DAL.Repository
{
    public interface IOrdemServicoRepository : IRepository<tblOrdemServico>
    {
        OrdemServico Cadastrar(OrdemServico os);
        int Editar(OrdemServico os);
        int Fechar(int ordemServicoId);
        int InicioAnalise(OrdemServico os);
        int FimAnalise(OrdemServico os);
        int Autorizacao(OrdemServico os);
        int Conserto(OrdemServico os);
        int Deslocamento(OrdemServico os);
        List<OrdemServico> Listar();
        OrdemServico GetOs(int osId);
    }
}
