﻿using Dao.Context;
using Model;
using Scos.DAO.Repository.Base;
using Scos.Entities;
using System.Collections.Generic;

namespace Scos.DAL.Repository
{
    public interface IChamadoRepository : IRepository<tblChamado>
    {
        Chamado Cadastrar(Chamado chamado);
        Chamado Editar(Chamado chamado);
        int Fechar(ChamadoUsuario chamadoUsuario);
        List<ChamadoMap> PinChamados();
        List<Chamado> Chamados();
        List<Chamado> ChamadosPorEmpresa(int empresaId);
        List<Chamado> ChamadosPorUsuario(int usuarioId);
        Chamado Chamado(int id);
    }
}
