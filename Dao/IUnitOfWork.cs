﻿using Scos.DAL.Repository;
using System;

namespace Scos.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IChamadoRepository Chamado { get; }
        IGeografiaRepository Geografia { get; }
        IEmpresaRepository Empresa { get; }
        IUsuarioRepository Usuario { get; }
        IItemRepository Item { get; }
        IOrdemServicoRepository OrdemServico { get; }
        int Commit();
    }
}
