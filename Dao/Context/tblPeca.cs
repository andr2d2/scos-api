//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dao.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPeca
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblPeca()
        {
            this.tblPecaOrdemServico = new HashSet<tblPecaOrdemServico>();
        }
    
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Peca { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblPecaOrdemServico> tblPecaOrdemServico { get; set; }
    }
}
