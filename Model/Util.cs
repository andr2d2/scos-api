﻿using System;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using Scos.Entities;

namespace Dao
{
    public class Util
    {
        public int EnviarEmailNovaOrdemServico(OrdemServico os)
        {
            try
            {
                var staticUrl = ConfigurationSettings.AppSettings["static"].ToString();
                string email = new WebClient().DownloadString(string.Format("{0}EmailCadastroOs.html", staticUrl));

                var sb = new StringBuilder(email);
                sb.Replace("[osId]", os.id.ToString());
                sb.Replace("[tipo]", os.tipo.tipo.ToString());
                sb.Replace("[preventivo]", (os.preventivo ? "Sim" : "Não"));
                sb.Replace("[emergencia]", (os.emergencia ? "Sim" : "Não"));
                sb.Replace("[status]", os.status.status.ToString());
                sb.Replace("[nomeCompleto]", os.usuario.nomeCompleto.ToString());
                sb.Replace("[empresa]", os.empresa.nomeFantasia.ToString());
                sb.Replace("[defeitoInformado]", os.defeitoInformado.ToString());
                sb.Replace("[defeitoConstatado]", os.defeitoInformado.ToString());
                sb.Replace("[scosUrl]", ConfigurationSettings.AppSettings["scosUrl"].ToString());
                sb.Replace("[dataEnvio]", DateTime.Now.ToLongDateString());

                sb.Replace("[dataAbertura]", os.dataAberturaOs.ToLocalTime().ToString());

                if (os.equipamentos.Count > 0)
                {
                    foreach (var item in os.equipamentos)
                    {
                        var itemString = new StringBuilder();
                        itemString.Append("<hr>");
                        itemString.Append("<ul>");
                        itemString.Append(string.Format("<li>Equipamento: {0}</li>", item.equipamento));
                        itemString.Append(string.Format("<li>Modelo: {0}</li>", item.modelo));
                        itemString.Append(string.Format("<li>Número de série: {0}</li>", item.numeroSerie));
                        itemString.Append("</ul>");
                        sb.Replace("[equipamentos]", itemString.ToString());
                    }
                }
                else
                {
                    sb.Replace("[equipamentos]", "Não há equipamento informado");
                }


                EnviarEmail("SCOS - Nova Ordem de Serviço", sb.ToString());

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int EnviarEmailAutorizacao(OrdemServico os)
        {
            try
            {
                var staticUrl = ConfigurationSettings.AppSettings["static"].ToString();
                string email = new WebClient().DownloadString(string.Format("{0}EmailAutorizacao.html", staticUrl));

                var sb = new StringBuilder(email);
                sb.Replace("[osId]", os.id.ToString());
                sb.Replace("[autorizacao]", (os.autorizacao.autorizacao.Value ? "AUTORIZADA" : "NÃO autorizada"));
                sb.Replace("[dataAutorizacao]", os.autorizacao.dataAutorizacao.Value.ToLocalTime().ToString());
                sb.Replace("[responsavel]", os.autorizacao.responsavelAutorizacao.ToString());


                EnviarEmail("Nova ordem de serviço", sb.ToString());

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public int EnviarEmailEncerramentoOs(OrdemServico os)
        {
            try
            {
                var staticUrl = ConfigurationSettings.AppSettings["static"].ToString();
                string email = new WebClient().DownloadString(string.Format("{0}EmailEncerramentoOs.html", staticUrl));

                var sb = new StringBuilder(email);
                sb.Replace("[osId]", os.id.ToString());
                sb.Replace("[dataFechamento]", os.dataFechamentoOs.Value.ToLocalTime().ToString());

                EnviarEmail("Nova ordem de serviço", sb.ToString());

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }


        public int EnviarEmail(string assunto, string corpo)
        {
            try
            {
                var smtp = ConfigurationSettings.AppSettings["mySmtpClient"].ToString();
                var port = Convert.ToInt32(ConfigurationSettings.AppSettings["port"]);
                SmtpClient mySmtpClient = new SmtpClient(smtp, port);//my.smtp.exampleserver.net

                // set smtp-client with basicAuthentication
                mySmtpClient.UseDefaultCredentials = false;

                var username = ConfigurationSettings.AppSettings["username"].ToString();
                var password = ConfigurationSettings.AppSettings["password"].ToString();

                NetworkCredential basicAuthenticationInfo = new NetworkCredential(username, password);
                mySmtpClient.Credentials = basicAuthenticationInfo;
                mySmtpClient.EnableSsl = false;

                // add from,to mailaddresses
                var fromAdress = ConfigurationSettings.AppSettings["from"].ToString();
                var toAdress = ConfigurationSettings.AppSettings["to"].ToString();
                var myMailAdress = ConfigurationSettings.AppSettings["myMail"].ToString();

                MailAddress from = new MailAddress(fromAdress);
                MailAddress to = new MailAddress(fromAdress);
                MailMessage myMail = new MailMessage(fromAdress, toAdress);

                // add ReplyTo
                var replyTo = ConfigurationSettings.AppSettings["replyTo"].ToString();
                MailAddress replyto = new MailAddress(replyTo);
                myMail.ReplyToList.Add(replyTo);

                // set subject and encoding
                myMail.Subject = assunto;
                myMail.SubjectEncoding = Encoding.UTF8;

                // set body-message and encoding
                myMail.Body = corpo;
                myMail.BodyEncoding = Encoding.UTF8;
                // text or html
                myMail.IsBodyHtml = true;

                mySmtpClient.Send(myMail);

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public string TimestampToStringFormatada(DateTime fim, DateTime inicio)
        {
            var timestamp = fim.Subtract(inicio);

            var mes = 0;
            var dias = (int)timestamp.TotalDays;
            var hrs = (int)timestamp.TotalHours;
            var min = (int)timestamp.TotalMinutes;
            var sb = new StringBuilder();

            if (dias >= 30)
            {
                mes = dias / 30;
                dias = dias % 30;
                sb.Append(String.Format("~ {0} mês(es), ",mes));
            }

            if (dias >= 1)
                sb.Append(String.Format("{0} dia(s), ",dias));
            
            if (hrs >= 24)
                hrs = 0;

            if (min >= 60)
                min = min % 60;

            sb.Append(String.Format("{0} hrs e {1} min",
                hrs,
                min
               
            ));

            return sb.ToString();
        }
    }
}
