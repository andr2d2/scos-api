﻿using System;
using System.Collections.Generic;

namespace Scos.Entities
{
    public class InicioAnaliseOs
    {
        public string nomeTecnicoInicioAnalise { get; set; }
        public int? tecnicoInicioAnalise { get; set; }
        public DateTime? dataInicioAnalise { get; set; }        
    }

    public class FimAnaliseOs
    {
        public string nomeTecnicoFimAnalise { get; set; }
        public int? tecnicoFimAnalise { get; set; }
        public DateTime? dataFimAnalise { get; set; }
        public List<Peca> pecas { get; set; }
        public string descricaoAnalise { get; set; }
    }

    public class Autorizacao
    {
        public bool? autorizacao { get; set; }
        public string responsavelAutorizacao { get; set; }
        public DateTime? dataAutorizacao { get; set; }
    }

    public class Conserto
    {
        public string nomeTecnicoConserto { get; set; }
        public int? tecnicoConserto { get; set; }
        public string descricaoConserto { get; set; }
        public DateTime? dataConserto { get; set; }
    }

    public class Deslocamaneto
    {
        public DateTime? deslocamentoSaida { get; set; }
        public DateTime? deslocamentoChegada { get; set; }
        public DateTime? deslocamentoTotal { get; set; }
        public double? kmInicial { get; set; }
        public double? kmFinal { get; set; }
        public double? kmTotal { get; set; }
    }
}