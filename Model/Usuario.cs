﻿namespace Scos.Entities
{
    public class Usuario
    {
        public int id { get; set; }
        public string cpf { get; set; }
        public string nomeCompleto { get; set; }
        public string email { get; set; }
        public string telContato { get; set; }
        public TipoUsuario tipoUsuario { get; set; }
        public Empresa empresa { get; set; }
        public bool ativo { get; set; }
    }
}