﻿using System;
using System.Collections.Generic;

namespace Scos.Entities
{
    public class Chamado
    {
        public int id { get; set; }
        public string descricao { get; set; }
        public DateTime dataAbertura { get; set; }
        public DateTime? dataFechamento { get; set; }
        public List<Item> itens { get; set; }
        public Usuario usuario { get; set; }
        public List<OrdemServico> ordensServico { get; set; }
        public bool ativo { get; set; }
        public Status status { get; set; }
    }
}
