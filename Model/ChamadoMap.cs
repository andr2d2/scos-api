﻿using Scos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ChamadoMap
    {
        public int chamadoId { get; set; }
        public Empresa empresa { get; set; }
        public Status status { get; set; }
    }
}
