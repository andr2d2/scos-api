﻿namespace Scos.Entities
{
    public class TipoUsuario
    {
        public int id { get; set; }
        public string tipo { get; set; }

        public enum enumTipoUsuario
        {
            Dev = 0,
            Administrador = 1,
            Cliente = 2,
            Técnico = 3
        }
    }
}