﻿using System;
using System.Collections.Generic;

namespace Scos.Entities
{
    public class OrdemServico
    {
        public int id { get; set; }
        public Status status { get; set; }
        public Usuario usuario { get; set; }
        public Empresa empresa { get; set; }
        //public int chamadoId { get; set; }

        public DateTime dataAberturaOs { get; set; }
        public DateTime? dataFechamentoOs { get; set; }
        public string tempoAberto { get; set; }

        public TipoOrdemServico tipo { get; set; }
        public bool preventivo { get; set; }
        public bool emergencia { get; set; }

        public List<Item> equipamentos { get; set; }
        public string defeitoInformado { get; set; }
        public string defeitoConstatado { get; set; }

        public InicioAnaliseOs inicioAnaliseOs { get; set; }
        public FimAnaliseOs fimAnaliseOs { get; set; }
        public Autorizacao autorizacao { get; set; }
        public Conserto conserto { get; set; }
        public Deslocamaneto deslocamaneto { get; set; }
    }
}