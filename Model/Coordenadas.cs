﻿namespace Scos.Entities
{
    public class Coordenadas
    {
        public decimal latitude { get; set; }
        public decimal longitude{ get; set; }
    }
}
