﻿namespace Scos.Entities
{
    public class Status
    {
        public int id { get; set; }
        public string status { get; set; }


        public enum enumStatus
        {
            Aberto = 1,
            Atribuido = 2,
            Pendente_autorizacao = 3,
            Autorizado = 4,
            Nao_autorizado = 5,
            Encerrado = 6,
            Recuperado = 7
        }
    }
}
