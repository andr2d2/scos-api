﻿namespace Scos.Entities
{
    public class Item
    {
        public int id { get; set; }
        public string equipamento { get; set; }
        public string numeroSerie { get; set; }
        public string modelo { get; set; }
        public string localizacao { get; set; }
    }
}
